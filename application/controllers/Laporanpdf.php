<?php
Class Laporanpdf extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->library('pdf');
    }
    
    function index(){
        $pdf = new FPDF('l','mm','A5');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        // mencetak string 
        $pdf->Cell(180,7,'KARTU PASIEN DENTAL CLINIC',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(180,7,'DAFTAR SISWA KELAS IX JURUSAN REKAYASA PERANGKAT LUNAK',0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','B',10);
        // $pdf->Cell(20,6,'NIM',1,0);
        // $pdf->Cell(85,6,'NAMA MAHASISWA',1,0);
        // $pdf->Cell(27,6,'NO HP',1,0);
        // $pdf->Cell(28,6,'TANGGAL LHR',1,1);
        // $pdf->SetFont('Arial','',10);
        $mahasiswa = $this->db->get('data_obat')->result();
        foreach ($mahasiswa as $cell){

            $pdf->Cell(20,6,$cell->id_obat,1,0);
            $pdf->Cell(85,6,$cell->id_dental,1,0);
            $pdf->Cell(27,6,$cell->nama_obat,1,0);
            $pdf->Cell(28,6,$cell->stock_obat,1,1);
             
        }
        $pdf->Output();
    }
}