<?php
date_default_timezone_set("Asia/Jakarta");
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugas extends CI_Controller {


	public function index(){

		$id_dental = $this->session->id_dental;

		$getDataPasien = $this->db->get_where('pasien', ['id_dental' => $id_dental])->num_rows();
		$getTotalTr = $this->db->like('input_tgl', date('Y-m-d'))->where('transaksi', 1)->get('antrian')->num_rows();
		$dataDental = $this->db->get_where('data_dental', ['id_dental' => $id_dental])->row();
		$data['jumlahpasien'] = $getDataPasien;
		$data['totaltransaksi'] = $getTotalTr;
		$data['petugas'] = $this->db->join('user', 'user.id_petugas = petugas.id_petugas')->get_where('petugas', ['petugas.id_dental' => $id_dental])->result();
		$data['data_dental'] = $dataDental;
		

		$this->load->view('component/v_header');
		$this->load->view('petugas', $data);
		$this->load->view('component/v_footer');
	}

	public function add_petugas(){
		$id_dental = $this->session->id_dental;
		$dataDental = $this->db->get_where('data_dental', ['id_dental' => $id_dental])->row();
		$data['data_dental'] = $dataDental;

		$this->load->view('component/v_header');
		$this->load->view('add_petugas', $data);
		$this->load->view('component/v_footer');

	}


	public function testflash(){
		$y = $this->session->flashdata();
		var_dump($y);
	}



	public function act_add_petugas(){
		$id_dental = $this->session->id_dental;

		$nama_petugas = $this->input->post('nama_petugas');
		$alamat_petugas = $this->input->post('alamat_petugas');
		$no_telp = $this->input->post('no_telp');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$jenis_petugas = $this->input->post('jenis_petugas');


		if($nama_petugas == ''){
			$nama_petugas = null;
		}elseif ($alamat_petugas == '') {
			$alamat_petugas = null;
		}elseif ($no_telp == '') {
			$no_telp = null;
		}elseif ($username == '') {
			$username = null;
		}elseif ($password == '') {
			$password = null;
		}elseif ($jenis_petugas == '') {
			$jenis_petugas = null;
		}


		$data = [
			'nama_pekerja' => $nama_petugas,
			'alamat_pekerja' => $alamat_petugas,
			'no_telp' => $no_telp,
			'id_dental' => $id_dental,
			'input_tgl' => date('Y-m-d h:i:s')
			];

		$add_petugas = $this->users->add_petugas($data);

		if($add_petugas === "add data gagal"){
			$this->session->set_flashdata('sukses_add', 'gagal');
			return redirect(base_url('Petugas/add_petugas'));

		}else{

			$idPetugas = $this->db->get('petugas')->result();


			$data_user = [
					'id_dental' => $id_dental,
					'nama' => $nama_petugas,
					'username' => $username,
					'id_petugas' => end($idPetugas)->id_petugas,
					'password' => md5($password),
					'role_user' => $jenis_petugas,
					'tgl_registrasi' => date('Y-m-d h:i:s')
					];

			$this->users->add_user($data_user);

			$this->session->set_flashdata('sukses_add', 'sukses');
			return redirect(base_url('Petugas'));
		}
	}

}
