<?php
date_default_timezone_set("Asia/Jakarta");
defined('BASEPATH') OR exit('No direct script access allowed');

class Anamnesa extends CI_Controller {

	public function index($id){
		$data['users'] = ['id_antrian' => $id];
		$this->load->view('anamnesa', $data);
	}

	public function add_anamnesa($id){
		$id_dental = $this->session->id_dental;
		$anamnesa = $this->input->post('anamnesa');

		if($anamnesa == ''){

			$anamnesa = null;
		}

		$rep = str_replace("\n", '<br>', $anamnesa);


		$data = [
			'id_dental' => $id_dental,
			'anamnesa' => $rep,
			'input_tgl' => date('Y-m-d h:i:s'),
			'id_transaksi' => $id
			];

		$insert = $this->pasien->add_anamnesa($data);


		if($insert == "add anamnesa berhasil"){
			$this->session->set_flashdata('sukses_add',  'sukses');
			$this->db->set(['anamnesa' => '1'])->where('id_antrian', $id)->update('antrian');
			return redirect('Dashboard');
		}else{
			$this->session->set_flashdata('sukses_add', 'gagal');
			return redirect('Dashboard');
		}
	}


}
