<?php
date_default_timezone_set("Asia/Jakarta");
defined('BASEPATH') OR exit('No direct script access allowed');

class Antrian extends CI_Controller {

	public function index(){

		$id_dental = $this->session->id_dental;
		$data['antrian'] = $this->db->where('id_dental', $id_dental)->like('input_tgl', date('Y-m-d'))->get('antrian')->result();

		$this->load->view('antrian', $data);
	}
}
