<?php
date_default_timezone_set("Asia/Jakarta");
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->load->view('landing/index');
	}

	/*
		Login
	*/
	public function login(){
		if(isset($this->session->id_dental)){
			return redirect(base_url('Dashboard'));
		}else{
			$this->load->view('auth/login');
		}
	}

	public function action_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$data = ['username' => $username, 'password' => $password]; 

		$login = $this->login->cek_login($data);

		if(is_array($login)){
			$set_session = [
				'id_dental' => $login['id_dental'],
				'id_user' => $login['id_user'],
				'id_petugas' => $login['id_petugas'],
				'nama' => $login['nama'],
				'role_user' => $login['role_user']
			];

			// print_r($set_session); die();
			$save = $this->session->set_userdata($set_session);
			redirect(base_url('dashboard'));
		}else{
			echo $login;
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		return redirect(base_url());
	}


	public function register(){
		$this->load->view('register');
	}

	public function act_register(){
		$nama_dental = $this->input->post('nama_dental');
		$nama_pemilik = $this->input->post('nama_pemilik');
		$alamat_dental = $this->input->post('alamat_dental');
		$email_dental = $this->input->post('email_dental');
		$telp_dental = $this->input->post('telp_dental');
		$tgl_registrasi = date('Y-m-d h:i:s');

		$cekEmail = $this->db->get_where('data_dental', ['email' => $email_dental])->row();

		// print_r($cekEmail); die;

		if($cekEmail == null){

			$data_dental = [
					'nama_dental' => $nama_dental,
					'nama_owner' => $nama_pemilik,
					'alamat_dental' => $alamat_dental,
					'email' => $email_dental,
					'no_telp' => $telp_dental,
					'tgl_registrasi' => $tgl_registrasi
					];

			$register = $this->pendaftaran->add_dental($data_dental);

			if($register == "add data berhasil"){
				$this->session->set_flashdata('register', 'sukses');
				return redirect(base_url('Auth/register'));
			}else{
				$this->session->set_flashdata('register', 'gagal');
				return redirect(base_url('Auth/register'));
			}

		}else{
			$this->session->set_flashdata('register', 'gagal');
			$this->session->set_flashdata('email_terdaftar', '1');
		}

	}

	public function add_pasien(){
		$nama_pasien = $this->input->post('nama_pasien');
		$kelamin = $this->input->post('kelamin');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$alamat = $this->input->post('alamat');
		$umur = $this->input->post('umur');
		$kontak = $this->input->post('kontak');
		$tgl = strtotime($tgl_lahir);
		$tanggal = date('d-m-Y', $tgl);


		$data = [
				'nama_pasien' => $nama_pasien,
				'alamat' => $alamat,
				'umur' => $umur,
				'gender' => $kelamin,
				'tanggal_lahir' => $tanggal,
				'kontak_pasien' => $kontak
			];

		$add = $this->pendaftaran->add_data($data);

		echo $add;
	}
}
