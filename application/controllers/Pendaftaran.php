<?php
date_default_timezone_set("Asia/Jakarta");
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftaran extends CI_Controller {

	public function index(){
		$id_dental = $this->session->id_dental;
		$dataDental = $this->db->get_where('data_dental', ['id_dental' => $id_dental])->row();
		$data['data_dental'] = $dataDental;

		$this->load->view('component/v_header');
		$this->load->view('pendaftaran', $data);
		$this->load->view('component/v_footer');
	}


	public function add_pasien(){
		$nama_pasien = $this->input->post('nama_pasien');
		$kelamin = $this->input->post('kelamin');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$agama = $this->input->post('agama');
		$nama_ibu = $this->input->post('nama_ibu');
		$telp_keluarga = $this->input->post('telp_keluarga');
		$alamat = $this->input->post('alamat');
		$umur = $this->input->post('umur');
		$kontak = $this->input->post('kontak');
		$tgl = strtotime($tgl_lahir);
		$tanggal = date('d-m-Y', $tgl);

		$id_dental = $this->session->id_dental;
		$input_oleh = $this->session->nama;
		// var_dump($agama); die;
		$getIdPasien = $this->db->get_where('pasien', ['id_dental' => $id_dental])->result();

		if($getIdPasien != null){
			$id_pasien = end($getIdPasien)->id_pasien + 1;
		}else{
			$id_pasien = 1;
		}

		$data = [
				'id_dental' => $id_dental,
				'id_pasien' => $id_pasien,
				'nama_ibu' => $nama_ibu,
				'kontak_keluarga' => $telp_keluarga,
				'agama' => $agama,
				'nama_pasien' => $nama_pasien,
				'alamat' => $alamat,
				'umur' => $umur,
				'gender' => $kelamin,
				'tanggal_lahir' => $tanggal,
				'kontak_pasien' => $kontak,
				'input_oleh' => $input_oleh,
				'input_tgl' => date('Y-m-d h:i:s')
			];

		$add = $this->pendaftaran->add_data($data);

		if($add == "add data berhasil"){
			$this->session->set_flashdata(['sukses_add' => 'sukses']);
			return redirect('Pasien');
		}else{
			$this->session->set_userdata(['sukses_add' => 'gagal']);
		}
	}
	public function add_antrian($id_pasien){
		$id_dental = $this->session->id_dental;
		$id_petugas = $this->session->id_petugas;

		$cek_pasien = $this->db->get_where('pasien', ['id_pasien' => $id_pasien, 'id_dental' => $id_dental])->row();
		$cekdate = $this->db->where('id_dental', $id_dental)->like('input_tgl', date('Y-m-d'))->get('antrian')->result();

			if($cekdate){
				$nomor_antrian = end($cekdate)->nomor_antrian + 1;
			}else{
				$nomor_antrian = 1;
			}

			if($cek_pasien != null){
						$data = [
							'id_pasien' => $id_pasien,
							'id_data_pasien' => $cek_pasien->id_data_pasien,
							'nomor_antrian' => $nomor_antrian, 
							'id_dental' => $id_dental,
							'id_petugas' => $id_petugas,
							'nama_pasien' => $cek_pasien->nama_pasien,
							'input_tgl' => date('Y-m-d h:i:s')
							];

				$ambilnomor = $this->pendaftaran->add_antrian($data);
				if($ambilnomor == "add antrian berhasil"){

					$getantrian = $this->db->get_where('antrian', ['id_dental' => $id_dental])->result();
					$nomorAntrian = end($getantrian)->nomor_antrian;
					$this->session->set_flashdata(['sukses_add' => 'sukses', 'nomor_antrian' => $nomorAntrian]);
					return redirect('Pasien');

				}else{
					$this->session->set_flashdata(['sukses_add' => 'gagal', 'nomor_antrian' => '0']);
					return redirect('Pasien');
				}


			}else{
				$this->session->set_flashdata(['sukses_add' => 'gagal', 'nomor_antrian' => '0']);
					return redirect('Pasien');
			}
	}

}