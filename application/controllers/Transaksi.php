<?php
date_default_timezone_set("Asia/Jakarta");
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {

	public function index(){
		$id_dental = $this->session->id_dental;
		// echo $this->session->role_user; die;
		$getDataPasien = $this->db->get_where('pasien', ['id_dental' => $id_dental])->num_rows();
		$getTotalTr = $this->db->like('input_tgl', date('Y-m-d'))->where('transaksi', 1)->get('antrian')->num_rows();
		$dataDental = $this->db->get_where('data_dental', ['id_dental' => $id_dental])->row();
		// print_r($getTotalTr); die;
		$data['jumlahpasien'] = $getDataPasien;
		$data['totaltransaksi'] = $getTotalTr;
		$data['data_dental'] = $dataDental;


		$id_dental = $this->session->id_dental;
		$data['data_transaksi'] = $this->db->join('obat_pasien', 'obat_pasien.id_transaksi = antrian.id_antrian')
											->join('pasien', 'pasien.id_data_pasien = antrian.id_data_pasien')
											->join('tindakan', 'tindakan.id_transaksi = antrian.id_antrian')
											->join('master_tindakan', 'master_tindakan.id_master = tindakan.id_master_tindakan')
											->like('antrian.input_tgl', date('Y-m-d'))
											->get_where('antrian', ['antrian.id_dental' => $id_dental, 'antrian.obat' => '1', 'anamnesa' => '1', 'antrian.tindakan' => '1', 'antrian.transaksi' => '0'])->result();
		// print_r($data['data_transaksi']); die;

		$this->load->view('component/v_header');
		$this->load->view('transaksi', $data);
		$this->load->view('component/v_footer');

	}


	public function tr_done($id){
		$tipe = $this->input->post('tipe_pembayaran');

		if($tipe == null){
			$this->session->set_flashdata('sukses_add', 'gagal');
			return redirect('Transaksi');			
		}

		$data_transaksi = $this->db->join('obat_pasien', 'obat_pasien.id_transaksi = antrian.id_antrian')
											->join('pasien', 'pasien.id_data_pasien = antrian.id_data_pasien')
											->join('tindakan', 'tindakan.id_transaksi = antrian.id_antrian')
											->join('master_tindakan', 'master_tindakan.id_master = tindakan.id_master_tindakan')
											->like('antrian.input_tgl', date('Y-m-d'))
											->get_where('antrian', ['antrian.id_antrian' => $id])->row();



		if(count($tipe) == 1){
			
			if($tipe[0] == 'tindakan'){
				$rep_biaya = str_replace('.', '', $data_transaksi->biaya_tindakan);
				$total_pembayaran = (int)$rep_biaya;
			}else{
				$total_pembayaran = $data_transaksi->total_harga;
			}

		}else{
			$rep_biaya = str_replace('.', '', $data_transaksi->biaya_tindakan);
			$total_pembayaran = (int)$rep_biaya + $data_transaksi->total_harga;
		}


		$update = $this->db->set(['transaksi' => 1, 'total_transaksi' => $total_pembayaran])->where('id_antrian', $id)->update('antrian');

		if($update == TRUE){
			$this->session->set_flashdata('sukses_add', 'sukses');
			return redirect('Transaksi');
		}else{
			$this->session->set_flashdata('sukses_add', 'gagal');
			return redirect('Transaksi');
		}
	}

	public function bagi_hasil(){

		$id_dental = $this->session->id_dental;


		$getDataPasien = $this->db->get_where('pasien', ['id_dental' => $id_dental])->num_rows();
		$getTotalTr = $this->db->like('input_tgl', date('Y-m-d'))->where('transaksi', 1)->get('antrian')->num_rows();
		$dataDental = $this->db->get_where('data_dental', ['id_dental' => $id_dental])->row();


		$data['jumlahpasien'] = $getDataPasien;
		$data['totaltransaksi'] = $getTotalTr;
		$data['data_dental'] = $dataDental;


		$id_dental = $this->session->id_dental;

		$data['data_petugas'] = $this->db->get_where('petugas', ['id_dental' => $id_dental])->result();

		$dataTr = [];

		foreach ($data['data_petugas'] as $IdPetugas) {
		

			if($IdPetugas->role_pekerja == 'Dokter'){

				$data['data_transaksi'] = $this->db->like('obat_pasien.input_tgl', date('Y-m'))
													->join('tindakan', 'tindakan.id_transaksi = obat_pasien.id_transaksi')
													->join('master_tindakan', 'master_tindakan.id_master = tindakan.id_master_tindakan')
													->get_where('obat_pasien', ['obat_pasien.id_dental' => $id_dental, ''])->result();

				foreach($data['data_transaksi'] as $DataTindakan){
					$totalTindakan = 0 + (int) str_replace('.', '', $DataTindakan->biaya_tindakan);
				}

				$IdPetugas->total = $totalTindakan;

			}else if($IdPetugas->role_pekerja == 'Petugas'){

				$data['data_transaksi'] = $this->db->like('antrian.input_tgl', date('Y-m'))
													->get_where('antrian', ['antrian.id_dental' => $id_dental, 'antrian.obat' => '1', 'anamnesa' => '1', 'antrian.tindakan' => '1', 'antrian.transaksi' => '1'])->result();

				$IdPetugas->total = count($data['data_transaksi']);

			}
		}


		// for($id=0; $id <= count($data['data_petugas']); $id++){

		// 	array_push($data['data_petugas'][$id], $dataTr[$id]);

		// }

		// echo "<pre>";
		// print_r($data['data_petugas']); die;

		$this->load->view('component/v_header');
		$this->load->view('bagi-hasil', $data);
		$this->load->view('component/v_footer');


	}


}
