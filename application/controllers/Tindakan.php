<?php
date_default_timezone_set("Asia/Jakarta");
defined('BASEPATH') OR exit('No direct script access allowed');

class Tindakan extends CI_Controller {

	public function index($id){
		$data['users'] = ['id_antrian' => $id];
		$this->load->view('tindakan', $data);
	}

	public function getmaster(){
		$id_dental = $this->session->id_dental;

		$getData['tindakan'] = $this->db->get_where('master_tindakan', ['id_dental' => $id_dental])->result();
		echo json_encode($getData['tindakan']);
	}

	public function add_tindakan($id){
		$id_dental = $this->session->id_dental;
		$id_tindakan = $this->input->post('tindakan');


		if($id_tindakan == ''){
			$id_tindakan = null;
		}else{
			$getTindakan = $this->db->get_where('master_tindakan', ['id_master' => $id_tindakan])->row();
		}

		$data = [
			'id_dental' => $id_dental,
			'id_master_tindakan' => $id_tindakan,
			'tindakan' => $getTindakan->nama_tindakan,
			'input_tgl' => date('Y-m-d h:i:s'),
			'id_transaksi' => $id
			];

		$insert = $this->pasien->add_tindakan($data);

		if($insert === "add tindakan berhasil"){
			$this->session->set_flashdata('sukses_add',  'sukses');
			$this->db->set(['tindakan' => '1'])->where('id_antrian', $id)->update('antrian');
			return redirect('Dashboard');
		}else{
			$this->session->set_flashdata('sukses_add',  'gagal');
			return redirect('Dashboard');
		}
	}


	public function add_master(){
		$id_dental = $this->session->id_dental;

		$nama_tindakan = $this->input->post('nama_tindakan');
		$biaya = $this->input->post('biaya_tindakan');

		$data = [
			'nama_tindakan' => $nama_tindakan,
			'biaya_tindakan' => $biaya,
			'id_dental' => $id_dental
			];

		// print_r($data); die;

		$input = $this->db->insert('master_tindakan', $data);

			if($input == TRUE){
				$this->session->set_flashdata('sukses_add',  'sukses');
				return redirect('Dashboard');
			}else{
				$this->session->set_flashdata('sukses_add',  'gagal');
				return redirect('Dashboard');
			}
	}
}
