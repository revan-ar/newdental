<?php
date_default_timezone_set("Asia/Jakarta");
defined('BASEPATH') OR exit('No direct script access allowed');
Class Pasien extends CI_Controller{


	public function index(){
		$id_dental = $this->session->id_dental;
		$getDataPasien = $this->db->get_where('pasien', ['id_dental' => $id_dental])->num_rows();
		$getTotalTr = $this->db->like('input_tgl', date('Y-m-d'))->where('transaksi', 1)->get('antrian')->num_rows();
		$dataDental = $this->db->get_where('data_dental', ['id_dental' => $id_dental])->row();
		$data = $this->data_table();
		$data['jumlahpasien'] = $getDataPasien;
		$data['totaltransaksi'] = $getTotalTr;
		$data['data_dental'] = $dataDental;

		$this->load->view('component/v_header');
		$this->load->view('pasien', $data);
		$this->load->view('component/v_footer');
	}

	public function rekamedis(){
		$this->load->view('component/v_header');
		$this->load->view('rekamedis');
		$this->load->view('component/v_footer');
	}


	public function data_table(){
		$id_dental = $this->session->id_dental;

		$getData = $this->db->get_where('pasien', ['id_dental' => $id_dental]);
		$data = [];
		$data["data"] = [];
		$output = [];

		foreach ($getData->result() as $dataPasien) {
			$dataAntrian = [$dataPasien->id_pasien, $dataPasien->nama_pasien, $dataPasien->alamat, $dataPasien->umur, $dataPasien->gender, $dataPasien->tanggal_lahir, $dataPasien->kontak_pasien, $dataPasien->id_data_pasien];
			$output[] = $dataAntrian;
		}

		$data['data'] = $output;
		return $data;
	}


	public function cetak($id){
		ob_start();
		$this->load->library('pdf');
		$id_dental = $this->session->id_dental;
		$getDataPasien = $this->db->get_where('pasien', ['id_dental' => $id_dental, 'id_pasien' => $id])->row();
		$getDataDental = $this->db->get_where('data_dental', ['id_dental' => $id_dental])->row();

		// print_r($getDataPasien); die;

		$data_transaksi = $this->db->select('*')
								->join('obat_pasien', 'obat_pasien.id_transaksi = antrian.id_antrian')
								->join('tindakan', 'tindakan.id_transaksi = antrian.id_antrian')
								->join('master_tindakan', 'master_tindakan.id_master = tindakan.id_master_tindakan')
								->join('anamnesa', 'anamnesa.id_transaksi = antrian.id_antrian')
								->join('pasien', 'pasien.id_data_pasien = antrian.id_pasien')
								->get_where('antrian', ['antrian.id_dental' => $id_dental, 'antrian.transaksi' => '1', 'antrian.id_pasien' => $id])->result();


		$id = 1;
		$data = '';

		if($getDataDental->logo_dental == null && $getDataDental->kop_surat == null){

			$logo = 'noimage.png';
			$kopSurat = 'no-image.jpg';

		}else if($getDataDental->logo_dental != null && $getDataDental->kop_surat == null){
			$kopSurat = 'no-image.jpg';
			$logo = $getDataDental->logo_dental;

		}else if($getDataDental->logo_dental == null && $getDataDental->kop_surat != null){
			$logo = 'noimage.png';
			$kopSurat = $getDataDental->kop_surat;
		}else if($getDataDental->logo_dental != null && $getDataDental->kop_surat != null){
			$logo = $getDataDental->logo_dental;
			$kopSurat = $kopSurat;
		}



		$output = '
		<html>
		<head>
			<title></title>
			<style type="text/css">
			td{
				padding: 10px;
			}

			img{
				margin-right: 20px;
			}
			</style>
		</head>
		<body>
			<img src="'.base_url('images/logo/').$logo.'" width="80px" height="80px" class="logo" />
			<img src="'.base_url('images/kop-surat/').$kopSurat.'" width="430px" height="80px" />
			<br><hr><hr><br>
				<h3 align="center">KARTU PASIEN<br></h3>
				<h5>Nama Lengkap: '.$getDataPasien->nama_pasien.'</h5>
				<h5>Tanggal Lahir: '.$getDataPasien->tanggal_lahir.'</h5>
				<h5>Agama: '.$getDataPasien->agama.'</h5>
				<h5>Alamat: '.$getDataPasien->alamat.'</h5>
				<h5>Umur: '.$getDataPasien->umur.'</h5>
				<h5>Jenis Kelamin: '.$getDataPasien->gender.'</h5>
				<h5>Nomor Telp: '.$getDataPasien->kontak_pasien.'</h5>
				<h5>Nomor Telp Keluarga: '.$getDataPasien->kontak_keluarga.'</h5>
		</body>
		</html>
		';

		// echo $output; die;



        $pdf = new TCPDF('L', PDF_UNIT, 'A5', true, 'UTF-8', false);
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        $pdf->setPrintFooter(false);
        $pdf->setPrintHeader(false);
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
		$pdf->AddPage();

		// output the HTML content
		$pdf->writeHTML($output);

		// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		// reset pointer to the last page
		$pdf->lastPage();

		// ---------------------------------------------------------

		//Close and output PDF document
		$pdf->Output('example_006.pdf', 'I');

	}

	public function add_logo(){

		$id_dental = $this->session->id_dental;

		$string = 'AaBcCDdEfFgHiJkLmMnNoOpPqQrRsStTuUvVwWxXyYzZ0123456789';
		$config['upload_path']          = './images/logo/';
		$config['file_name']			= md5(str_shuffle($string));
		$config['allowed_types']        = 'png|jpg';
		$config['max_size']             = 1024;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 800;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
        if ( ! $this->upload->do_upload('logodental')){
			$this->session->set_flashdata('sukses_add', 'gagal');
			return redirect('Pasien');
		}else{
			$this->session->set_flashdata('sukses_add', 'sukses');
			$this->db->set(['logo_dental' => $config['file_name'].'.png'])->where('id_dental', $id_dental)->update('data_dental');
			return redirect('Pasien');
    	}

	}


	public function add_kop_surat(){

		$exp = explode(".", $_FILES['kop_surat']['name']);

		if($exp[1] == "jpg"){
			$ext = ".jpg";
		}else{
			$ext = ".png";
		}

		$id_dental = $this->session->id_dental;

		$string = 'AaBcCDdEfFgHiJkLmMnNoOpPqQrRsStTuUvVwWxXyYzZ0123456789';
		$config['upload_path']          = './images/kop-surat/';
		$config['file_name']			= md5(str_shuffle($string));
		$config['allowed_types']        = 'png|jpg';
		$config['max_size']             = 1024;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 800;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
        if ( ! $this->upload->do_upload('kop_surat')){
			$this->session->set_flashdata('sukses_add', 'gagal');
			return redirect('Pasien');
		}else{
			$this->session->set_flashdata('sukses_add', 'sukses');
			$this->db->set(['kop_surat' => $config['file_name'].$ext])->where('id_dental', $id_dental)->update('data_dental');
			return redirect('Pasien');
    	}

	}

	public function cetak_rekamedis($id){
		ob_start();
		$this->load->library('pdf');
		$id_dental = $this->session->id_dental;
		$getDataPasien = $this->db->get_where('pasien', ['id_dental' => $id_dental, 'id_pasien' => $id])->row();
		$getDataDental = $this->db->get_where('data_dental', ['id_dental' => $id_dental])->row();

		$data_transaksi = $this->db->select('*')
								->join('obat_pasien', 'obat_pasien.id_transaksi = antrian.id_antrian')
								->join('tindakan', 'tindakan.id_transaksi = antrian.id_antrian')
								->join('master_tindakan', 'master_tindakan.id_master = tindakan.id_master_tindakan')
								->join('anamnesa', 'anamnesa.id_transaksi = antrian.id_antrian')
								->join('pasien', 'pasien.id_data_pasien = antrian.id_pasien')
								->get_where('antrian', ['antrian.id_dental' => $id_dental, 'antrian.transaksi' => '1', 'antrian.id_pasien' => $id])->result();


		// print_r($getDataDental); die;

		$id = 1;
		$data = '';

		if($getDataDental->logo_dental == null){
			$logo = 'noimage.png';
		}else{
			$logo = $getDataDental->logo_dental;
		}


		if($getDataDental->kop_surat == null){
			$kopSurat = 'no-image.jpg';
		}else{
			$kopSurat = $getDataDental->kop_surat;
		}

		if($getDataDental->logo_dental == null && $getDataDental->kop_surat == null){

			$logo = 'noimage.png';
			$kopSurat = 'no-image.jpg';

		}else if($getDataDental->logo_dental != null && $getDataDental->kop_surat == null){
			$kopSurat = 'no-image.jpg';
			$logo = $getDataDental->logo_dental;

		}else if($getDataDental->logo_dental == null && $getDataDental->kop_surat != null){
			$logo = 'noimage.png';
			$kopSurat = $getDataDental->kop_surat;
		}else if($getDataDental->logo_dental != null && $getDataDental->kop_surat != null){
			$logo = $getDataDental->logo_dental;
			$kopSurat = $kopSurat;
		}


		$output = '
		<html>
		<head>
			<title></title>
			<style type="text/css">
			table, td, tr{
				padding-left: 10px;
				padding-top: 8px;
				padding-bottom: 8px;
			}

			</style>
		</head>
		<body>
			<img src="'.base_url('images/logo/').$logo.'" width="80px" height="80px" style="margin-right: 5px" />
			<img src="'.base_url('images/kop-surat/').$kopSurat.'" width="430px" height="80px" />
			<br><hr><hr><br>
				<h3 align="center">RIWAYAT MEDIS<br></h3>
				<h5>Nama Lengkap: '.$getDataPasien->nama_pasien.'</h5>
				<h5>Tanggal Lahir: '.$getDataPasien->tanggal_lahir.'</h5>
				<h5>Agama: '.$getDataPasien->agama.'</h5>
				<h5>Alamat: '.$getDataPasien->alamat.'</h5>
				<h5>Umur: '.$getDataPasien->umur.'</h5>
				<h5>Jenis Kelamin: '.$getDataPasien->gender.'</h5>
				<h5>Nomor Telp: '.$getDataPasien->kontak_pasien.'</h5>
				<h5>Nomor Telp Keluarga: '.$getDataPasien->kontak_keluarga.'<br><br><br></h5>
		        <center>
		        	        <table border="1" width="100%">
		        	              <tr>
		        	                    <th style="text-align: center; font-weight: bold">Nomor</th>
		        	                    <th style="text-align: center; font-weight: bold">Anamnesa</th>
		        	                    <th style="text-align: center; font-weight: bold">Tindakan</th>
		        	                    <th style="text-align: center; font-weight: bold">Tanggal</th>
		        	              </tr>';
		        	              foreach ($data_transaksi as $dataTr) {
									$output .= '
									<tr>
									        <td align="center">'.$id++.'</td>
									        <td id="data">'.$dataTr->anamnesa.'</td>
									        <td id="data">'.$dataTr->nama_tindakan.'</td>
									        <td align="center">'.$dataTr->tgl_berobat.'</td>
									        </tr>';
									}
		        	              $output .= '
		        	        </table>
		        </center>
		</body>
		</html>
		';

		// echo $output; die;



        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        $pdf->setPrintFooter(false);
        $pdf->setPrintHeader(false);
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
		$pdf->AddPage();

		// output the HTML content
		$pdf->writeHTML($output);

		// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		// reset pointer to the last page
		$pdf->lastPage();

		// ---------------------------------------------------------

		//Close and output PDF document
		$pdf->Output('example_006.pdf', 'I');
	}

	public function print(){
		$url = base_url('Pasien/riwayat_medis/1');

		// echo $url;
		echo file_get_contents($url);

	}



}



?>