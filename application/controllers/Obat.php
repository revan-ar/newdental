<?php
date_default_timezone_set("Asia/Jakarta");
defined('BASEPATH') OR exit('No direct script access allowed');

class Obat extends CI_Controller {

	public function index(){
		$data = $this->data_table();
		$id_dental = $this->id_dental();
		$getDataPasien = $this->db->get_where('pasien', ['id_dental' => $id_dental])->num_rows();
		$getTotalTr = $this->db->like('input_tgl', date('Y-m-d'))->where('transaksi', 1)->get('antrian')->num_rows();
		$dataDental = $this->db->get_where('data_dental', ['id_dental' => $id_dental])->row();
		$data = $this->data_table();
		$data['jumlahpasien'] = $getDataPasien;
		$data['totaltransaksi'] = $getTotalTr;
		$data['jenis_obat'] = $this->db->get('master_obat')->result();
		$data['data_dental'] = $dataDental;

		// $getObat['obat'] = $this->db->get_where('data_obat', ['id_dental' => $id_dental])->result();

		$this->load->view('component/v_header');
		$this->load->view('obat', $data);
		$this->load->view('component/v_footer');
	}

	private function id_dental(){

		return $this->session->id_dental;
	}

	private function data_table(){
		$id_dental = $this->session->id_dental;

		$getData = $this->db->where('id_dental', $id_dental)->join('master_obat', 'master_obat.id_obat = data_obat.id_jenis_obat')->get('data_obat');
		$data = [];
		$data["data"] = [];
		$output = [];
		// echo "<pre>";
		// print_r($getData->result()); die;

		foreach ($getData->result() as $dataObat) {
			$dataAntrian = [$dataObat->id_obat, $dataObat->nama_obat, $dataObat->jenis_obat, $dataObat->stock_obat];
			$output[] = $dataAntrian;
		}
		// echo "<pre>";
		// print_r($output); die();
		$data['data'] = $output;
		// print_r($data);
		return $data;
	}

	// Add Master data obat

	public function add_master_obat(){

		$this->load->view('add-master-obat');

	}


	public function master_obat(){
		$id_dental = $this->id_dental();
		$data['obat'] = $this->db->get_where('master_obat', ['id_dental' => $id_dental])->result();
 		echo json_encode($data['obat']);
		// $this->load->view('add-obat-pasien', $data);
	}

	public function getmaster_obat(){
		$id_dental = $this->id_dental();
		$data['obat'] = $this->db->where('id_dental', $id_dental)->order_by('nama_obat', 'ASC')->get('data_obat')->result();
 		echo json_encode($data['obat']);
		// $this->load->view('add-obat-pasien', $data);
	}

	public function add_obat_pasien($id){
		$id_dental = $this->id_dental();
		$input_oleh = $this->session->id_petugas;
		$obat = $this->input->post('obat_pasien');
		$dosis = $this->input->post('dosis');
		

		$obat_pasien = [];
		$harga_obat = 0;
		$idDosis = 0;
		$datadosis = '';

		foreach ($obat as $obatPasien) {
			$getnamaObat = $this->db->get_where('data_obat', ['id_obat' => $obatPasien])->row();
			// print_r($getnamaObat); die;
			$kurangistock = $getnamaObat->stock_obat - 1;
			// $this->db->set(['stock_obat' => $kurangistock])->where('id_obat', $id)->update('data_obat');
			$obat_pasien[] = $getnamaObat->nama_obat;
			$harga_obat += (int) str_replace('.', '', $getnamaObat->harga_obat);
			$datadosis .= $getnamaObat->nama_obat.": ".$dosis[$idDosis++]."\n";
		}


		$obatpasien = implode($obat_pasien, ',');
		$dataAntrian = $this->db->get_where('antrian', ['id_antrian' => $id])->row();
		// print_r($dataAntrian); die
		$datapasien = $this->db->get_where('pasien', ['id_dental' => $id_dental, 'id_pasien' => $dataAntrian->id_pasien])->row();



		if($obatpasien == ""){
			$obatpasien = null;
		}else if($datadosis == ""){
			$datadosis = null;
		}


		$data = [
			'id_dental' => $id_dental,
			'id_pasien' => $dataAntrian->id_pasien,
			'nama_pasien' => $datapasien->nama_pasien,
			'alamat' => $datapasien->alamat,
			'obat' => $obatpasien,
			'dosis' => $datadosis,
			'total_harga' => $harga_obat,
			'tgl_berobat' => date('Y-m-d'),
			'input_tgl' => date('Y-m-d h:i:s'),
			'id_dokter' => $input_oleh,
			'id_transaksi' => $id
			];

		$input = $this->obat->add_obat_pasien($data);

		if($input == "add obat pasien berhasil"){
			$this->db->set(['obat' => '1'])->where('id_antrian', $id)->update('antrian');

			$this->session->set_flashdata('sukses_add', 'sukses');

			return redirect(base_url('Dashboard'));
		}else{
			$this->session->set_flashdata('sukses_add', 'gagal');
			return redirect(base_url('Dashboard'));
		}

	}

	// ADD DATA OBAT
	public function add_data(){
		$id_dental = $this->id_dental();
		$data['master'] = $this->db->get_where('master_obat', ['id_dental' => $id_dental])->result();

		$this->load->view('add-data-obat', $data);
	}


	// ADD Stock
	public function add_stock(){
		$id_dental = $this->id_dental();

		$stock = $this->input->post('stock_obat[]');
		$getData = $this->db->get_where('data_obat', ['id_dental' => $id_dental])->result();

		$id = 0;
		foreach ($getData as $dataObat) {
			
			$this->db->set(['stock_obat' => $stock[$id++]])->where('id_obat', $dataObat->id_obat)->update('data_obat');
		}

			$this->session->set_flashdata('sukses_add', 'sukses');
			return redirect(base_url('Obat'));
	}



	public function act_add_data(){
		$id_dental = $this->id_dental();
		$nama_obat = $this->input->post('nama_obat');
		$jenis_obat = $this->input->post('jenis_obat');
		$stock_obat = $this->input->post('stock_obat');
		$harga_obat = $this->input->post('harga_obat');

		if($nama_obat == ""){
			$nama_obat = null;
		}elseif($harga_obat == ""){
			$harga_obat = null;
		}elseif($jenis_obat == ""){
			$jenis_obat = null;
		}elseif($stock_obat == ""){
			$stock_obat = null;
		}

		$data = [
			'id_dental' => $id_dental,
			'nama_obat' => $nama_obat,
			'id_jenis_obat' => $jenis_obat,
			'stock_obat' => $stock_obat,
			'input_tgl' => date('Y-m-d h:i:s')
			];

		$add = $this->db->insert('data_obat', $data);

		if($add == TRUE){
			$this->session->set_flashdata('sukses_add', 'sukses');
			return redirect(base_url('Obat'));
		}else{
			$this->session->set_flashdata('sukses_add', 'gagal');
			return redirect(base_url('Obat'));
		}
		
	}



	public function act_add_master(){
		$id_dental = $this->id_dental();
		$warna = $this->input->post('warna');

		$cekMaster = $this->db->get_where('master_obat', ['id_dental' => $id_dental, 'warna_obat' => $warna])->row();

		if($cekMaster != null){
			echo "gagal : master data ini sudah terdaftar";
		}else{
			$data = ['warna_obat' => $warna, 'id_dental' => $id_dental];
			echo $this->obat->add_master($data);
		}
	}
}
