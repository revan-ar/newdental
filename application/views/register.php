<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Pendaftaran App Dental</title>

  <!-- Custom fonts for this template-->
  <link href="../template/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="../template/https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../template/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5" style="background-image: url(<?= base_url('template/img/undraw_doctor_kw5l.png') ?>); background-repeat: no-repeat; background-size: cover; background-position: center center"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Pendaftaran App Dental</h1>
              </div>
              <form class="user" method="post" action="<?= base_url('auth/act_register') ?>">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" name="nama_dental" class="form-control form-control-user" id="exampleFirstName" placeholder="Nama Dental">
                  </div>
                  <div class="col-sm-6">
                    <input type="text" name="nama_pemilik" class="form-control form-control-user" id="exampleLastName" placeholder="Nama Pemilik">
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" name="alamat_dental" class="form-control form-control-user" id="exampleInputEmail" placeholder="Alamat Dental">
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="email" name="email_dental" class="form-control form-control-user" id="exampleInputPassword" placeholder="Email">
                  </div>
                  <div class="col-sm-6">
                    <input type="text" name="telp_dental" class="form-control form-control-user number" id="exampleRepeatPassword" placeholder="No Telpon">
                  </div>
                </div>
                <button type="submit" class="btn btn-primary btn-user btn-block">
                  Daftar
                </button>
                <hr>
              </form>
              <hr>
              <div class="text-center">
                <a class="small" href="forgot-password.html">Forgot Password?</a>
              </div>
              <div class="text-center">
                <a class="small" href="login.html">Sudah pernah mendaftar? Login!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="../template/vendor/jquery/jquery.min.js"></script>
  <script src="../template/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="../template/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../template/js/sb-admin-2.min.js"></script>

  <script>
    $(function(){
       $(".number").keypress(function (e) {
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
          $("#errmsg").html("Number Only").stop().show().fadeOut("slow");
          return false;
        }
       });
    });


    <?php if($this->session->flashdata() != null) {

              if($this->session->flashdata('register') == 'sukses'){ ?>

                  Swal.fire(
                    'success !!',
                    'Registrasi Berhasil, Silahkan Menunggu Email Konfirmasi Dari Kami',
                    'success'
                  );


                  <?php }else if($this->session->flashdata('register') == 'gagal' && $this->session->flashdata('email_terdaftar') != '0'){ ?>

                        Swal.fire(
                          'Registrasi Gagal',
                          'Email Telah Terdaftar',
                          'error'
                        );


                  <?php }else if($this->session->flashdata('register') == 'gagal'){ ?>

                        Swal.fire(
                          'gagal !!',
                          'Submit data gagal',
                          'error'
                        );                          


                  <?php } ?>

      <?php
          }
      ?>
  </script>
</body>

</html>
