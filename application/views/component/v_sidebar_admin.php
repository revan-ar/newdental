    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center" href="index.html">
        <div class="sidebar-brand-icon">
          <i class="fa fa-tooth"></i>
        </div>
        <div class="sidebar-brand-text mx-1">Dashboard Dental</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item" id="MenuDashboard">
        <a class="nav-link" href="<?= base_url().'Dashboard' ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Halaman
      </div>

      <!-- Nav Item - Pages Collapse Menu -->

      <!-- Nav Item - Pages Collapse Menu -->
<!--       <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Pages</span>
        </a> -->
<!--         <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6>
            <a class="collapse-item" href="login.html">Login</a>
            <a class="collapse-item" href="register.html">Register</a>
            <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
            <div class="collapse-divider"></div>
            <h6 class="collapse-header">Other Pages:</h6>
            <a class="collapse-item" href="404.html">404 Page</a>
            <a class="collapse-item" href="blank.html">Blank Page</a>
          </div>
        </div> -->
      </li>

      <!-- Nav Item - Charts -->
      <li class="nav-item" id="MenuObat">
        <a class="nav-link" href="<?= base_url('Obat') ?>">
          <i class="fas fa-fw fa-tablets"></i>
          <span>Obat</span></a>
      </li>

      <li class="nav-item" id="MenuObat">
        <a class="nav-link" href="<?= base_url('Obat') ?>">
          <i class="fas fa-fw fa-database"></i>
          <span>Data Dental</span></a>
      </li>

      <li class="nav-item" id="MenuVerifikasi">
        <a class="nav-link" href="<?= base_url('Dashboard/verifikasi') ?>">
          <i class="fas fa-fw fa-user-check"></i>
          <span>Verifikasi</span></a>
      </li>

      <!-- Nav Item - Tables -->
      <li class="nav-item" id="MenuPetugas">
        <a class="nav-link" href="tables.html">
          <i class="fas fa-fw fa-user"></i>
          <span>Karyawan</span></a>
      </li>
      <!-- Nav Item - Pasien -->
      <li class="nav-item" id="MenuPasien">
        <a class="nav-link" href="<?= base_url('Pasien') ?>">
          <i class="fas fa-procedures"></i>
          <span>Pasien</span></a>
      </li>
      <!--  -->
      <li class="nav-item" id="MenuPendaftaran">
        <a class="nav-link" href="<?= base_url('Pendaftaran') ?>">
          <i class="fas fa-plus-circle"></i>
          <span>Pendaftaran</span></a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->