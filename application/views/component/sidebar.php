    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center" href="index.html">
        <div class="sidebar-brand-icon">
          <i class="fa fa-tooth"></i>
        </div>
        <div class="sidebar-brand-text mx-1">Dashboard Dental</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url().'Dashboard' ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Halaman
      </div>




      </li>

      <!-- Nav Item - Charts -->
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('Obat') ?>">
          <i class="fas fa-fw fa-tablets" style="color: white"></i>
          <span>Obat</span></a>
      </li>

      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('Petugas') ?>">
          <i class="fas fa-fw fa-user" style="color: white"></i>
          <span>Karyawan</span></a>
      </li>
      <!-- Nav Item - Pasien -->
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('Pasien') ?>">
          <i class="fas fa-procedures" style="color: white"></i>
          <span>Pasien</span></a>
      </li>
      <!--  -->
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('Pendaftaran') ?>">
          <i class="fas fa-plus-circle" style="color: white"></i>
          <span>Pendaftaran</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('Dashboard/add_tindakan') ?>">
          <i class="fas fa-plus" style="color: white"></i>
          <span>Tambah Tindakan</span></a>
      </li>


      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('Petugas/add_petugas') ?>">
          <i class="fas fa-plus" style="color: white"></i>
          <span>Tambah Karyawan</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->