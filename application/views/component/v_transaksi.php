          <div class="row">

              <div class="modal fade" id="modalTransaksi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Detail Transaksi</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="mb-4">
                      <p class="text text-lg">Nama Pasien: <span class="text-sm" id="NamaPasien"></span></p> 
                      </div>
                      <div class="mb-4">
                      <p class="text text-lg">Alamat: <span class="text-sm" id="AlamatPasien"></span></p>
                      </div>
                      <div class="mb-4">
                      <p class="text text-lg">Obat Pasien: <span class="text-sm" id="ObatPasien"></span></p>
                      </div>
                      <div class="mb-4">
                      <p class="text text-lg">Dosis Obat: <br><span id="DosisObat"></spam></p>
                      </div>
                      <div class="mb-4">
                      <p class="text text-lg">Harga Obat: <span class="text-sm" id="HargaObat"></span></p>
                      </div>
                      <div class="mb-4">
                      <p class="text text-lg">Tindakan Dokter: <span class="text-sm" id="TindakanDokter"></span></p>
                      </div>
                      <div class="mb-4">
                      <p class="text text-lg">Total Biaya: <span class="text-sm" id="TotalBiaya"></span></p>
                      </div>
                      <form id="UrlTransaksi" action="" method="post">
                            <label class="mr-4 text-md">Tipe Pembayaran: </label>
                            <input type="checkbox" id="tindakan" name="tipe_pembayaran[]" value="tindakan">
                            <label class="text-sm mr-3" for="tindakan">Tindakan</label>
                            <input type="checkbox" id="obat" name="tipe_pembayaran[]" value="obat">
                            <label class="text-sm" for="tindakan">Obat</label>
                    </div>
                    <div class="modal-footer">
                        <!-- <div class="ml-auto"> -->
                        <!-- </div> -->
                      <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                          <button class="btn btn-sm btn-success mr-auto" type="submit" style="color: white">Tranksaksi Selesai</button>
                    </div>
                      </form>
                  </div>
                </div>
              </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-sm font-weight-bold text-primary text-uppercase mb-1">Tanggal</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800" id="get-time"></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-sm font-weight-bold text-success text-uppercase mb-1">Jumlah Pasien</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $jumlahpasien ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-procedures fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-sm font-weight-bold text-danger text-uppercase mb-1">transaksi hari ini</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totaltransaksi ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-credit-card fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="container-fluid">
              <!-- <div class="col-lg-6"> -->
              <div class="table-responsive">
                <table class="display table" id="DataTransaksi" style="width: 100%">
                  <thead>
                      <tr>
                          <th>Nomor</th>
                          <th>Nama Pasien</th>
                          <th>Alamat</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tfoot>
                      <tr>
                          <th>Nomor</th>
                          <th>Nama Pasien</th>
                          <th>Alamat</th>
                          <th>Action</th>
                      </tr>
                  </tfoot>
                  <tbody>
                    <?php $id = 1; ?>
                    <?php foreach ($data_transaksi as $transaksi) { ?>
                      <tr>
                        <td><?= $id++ ?></td>
                        <td><?= $transaksi->nama_pasien ?></td>
                        <td><?= $transaksi->alamat ?></td>
                        <td>
                          <button onclick="ShowTransaksi('<?= $transaksi->nama_pasien ?>', '<?= $transaksi->alamat ?>', '<?= $transaksi->obat ?>', '<?php $exDosis = explode("\n", $transaksi->dosis); $dosis = implode($exDosis, ","); echo $dosis ?>', '<?= $transaksi->total_harga ?>', '<?= $transaksi->tindakan ?>', '<?=  str_replace('.', '', $transaksi->biaya_tindakan) ?> + <?=  $transaksi->total_harga ?> = <?=  str_replace('.', '', $transaksi->biaya_tindakan) + $transaksi->total_harga ?>', '<?= $transaksi->id_antrian ?>')" class="btn btn-danger">DETAIL</button>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- </div> -->
            </div>
          </div>