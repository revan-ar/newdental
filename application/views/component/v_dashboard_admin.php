          <div class="row">

            <!-- Modal Anamnesa -->
              <div class="modal fade" id="modalanamnesa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="TitleModal"></h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form method="post" action="" id="form-anamnesa">
                        <input type="text" class="form-control mb-2" name="anamnesa" id="input-modal">
                        <button class="btn btn-primary" type="submit">Submit</button>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>

              <!-- Modal Obat -->
              <div class="modal fade" id="modalobat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="TitleModalObat">Obat Pasien</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form method="post" action="" id="form-obat">
                        <div class="input-group mb-2">
                          <select class="custom-select" id="SelectMaster">
                            <!-- <option></option> -->
                          </select>
                        </div>
                        <select name="obat_pasien" multiple class="form-control selectpicker" id="SelectObat">
                        <option value="1">test</option>
                        <option value="2">test1</option>
                        <option value="3">test2</option>
                        <option value="4">test3</option>
                        <option value="5">test4</option>
                        </select>
                        <button class="btn btn-primary" type="submit">Submit</button>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-sm font-weight-bold text-primary text-uppercase mb-1">Tanggal</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800" id="get-time"></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-sm font-weight-bold text-success text-uppercase mb-1">Jumlah Pasien</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $jumlahpasien ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-procedures fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-sm font-weight-bold text-danger text-uppercase mb-1">transaksi hari ini</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totaltransaksi ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-credit-card fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="container-fluid">
              <!-- <div class="col-lg-6"> -->
              <div class="table-responsive">
                <table class="display table" id="example" style="width: 100%">
                  <thead>
                      <tr>
                          <th>ID Dental</th>
                          <th>Nama Dental</th>
                          <th>Nama Pemilik</th>
                          <th>Nomor Telepon</th>
                          <th>Email</th>
                          <th>Alamat Dental</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tfoot>
                      <tr>
                          <th>ID Dental</th>
                          <th>Nama Dental</th>
                          <th>Nama Pemilik</th>
                          <th>Nomor Telepon</th>
                          <th>Email</th>
                          <th>Alamat Dental</th>
                          <th>Action</th>
                      </tr>
                  </tfoot>
                  <tbody>
                    <?php $id = 1; ?>
                    <?php foreach ($data_dental as $dataDental) { ?>
                      <tr>
                        <td><?= $id++ ?></td>
                        <td><?= $dataDental->nama_dental ?></td>
                        <td><?= $dataDental->nama_owner ?></td>
                        <td><?= $dataDental->no_telp ?></td>
                        <td><?= $dataDental->email ?></td>
                        <td><?= $dataDental->alamat_dental ?></td>
                        <td align="center">

                          <!-- Button Verifikasi -->
                          <?php if($dataDental->status_verifikasi == 0){ ?>
                            <a class="btn btn-success btn-sm" href="<?= base_url('Dashboard/verif_dental/'.$dataDental->id_dental) ?>">Verifikasi</A>
                          <?php } ?>                            
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- </div> -->
            </div>
          </div>