          <div class="row">
            <div class="container mb-4">              
              <div class="col-lg-6">
                <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Tambah Obat</button>
              </div>
            </div>
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-sm font-weight-bold text-primary text-uppercase mb-1">Tanggal</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800" id="get-time"></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-sm font-weight-bold text-success text-uppercase mb-1">Jumlah Pasien</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $jumlahpasien ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-procedures fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-sm font-weight-bold text-danger text-uppercase mb-1">transaksi hari ini</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totaltransaksi ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-credit-card fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Obat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form method="post" action="<?= base_url('Obat/act_add_data') ?>">

                      <label>Nama Obat</label><br>
                      <div class="form-group">
                        <input class="form-control" type="text" name="nama_obat">
                      </div>


                      <label>Stock Obat</label><br>
                      <div class="form-group">
                        <input class="form-control" type="number" name="stock_obat">
                      </div>


                      <label>Harga Obat</label><br>
                      <div class="form-group">
                        <input class="form-control" type="number" name="harga_obat">
                      </div>


                      <label>Jenis Obat</label><br>
                      <div class="form-group">
                        <select name="jenis_obat" class="form-control">
                            <option value=""></option>
                          <?php foreach ($jenis_obat as $jenis) { ?>
                            <option value="<?= $jenis->id_obat ?>"><?= $jenis->jenis_obat ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="container-fluid">
              <!-- <div class="col-lg-6"> -->
              <div class="table-responsive mb-3">
                <form method="post" action="<?= base_url('Obat/add_stock') ?>">
                  <table class="display" id="dataObat" style="width: 100%">
                    <thead>
                        <tr>
                            <th>ID Obat</th>
                            <th>Nama Obat</th>
                            <th>Jenis Obat</th>
                            <th>Stock Obat</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>ID Obat</th>
                            <th>Nama Obat</th>
                            <th>Jenis Obat</th>
                            <th>Stock Obat</th>
                        </tr>
                    </tfoot>
                    <tbody>
                      <?php $id = 1; ?>
                      <?php foreach ($data as $obat) { ?>
                        <tr>
                          <td><?= $id++ ?></td>
                          <td><?= $obat[1] ?></td>
                          <td><?= $obat[2] ?></td>
                          <td><input class="form-control number" type="text" name="stock_obat[]" value="<?= $obat[3] ?>"></td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </form>
              </div>
              <!-- </div> -->
            </div>
          </div>