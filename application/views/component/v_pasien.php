          <div class="row">
            <div class="col-lg-6 ml-auto mb-4">
              <form enctype="multipart/form-data" method="post" action="<?= base_url('Pasien/add_logo'); ?>">
                  <label class="font-weight-bold text-lg">Logo Dental</label><br>
                  <div class="custom-file form-group form-group-half">
                    <input type="file" name="logodental" class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">Logo Dental</label>
                  <button class="btn btn-sm btn-primary mt-2" type="submit">upload logo</button>
                  </div>
              </form>
            </div>

            <div class="col-lg-6 ml-auto mb-4">
              <form enctype="multipart/form-data" method="post" action="<?= base_url('Pasien/add_kop_surat'); ?>">
                  <label class="font-weight-bold text-lg">Kop Surat</label><br>
                  <div class="custom-file form-group form-group-half">
                    <input type="file" name="kop_surat" class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">Kop Surat</label>
                  <button class="btn btn-sm btn-primary mt-2" type="submit">upload Kop Surat</button>
                  </div>
              </form>
            </div>


            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-sm font-weight-bold text-primary text-uppercase mb-1">Tanggal</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800" id="get-time"></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-sm font-weight-bold text-success text-uppercase mb-1">Jumlah Pasien</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $jumlahpasien ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-procedures fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-sm font-weight-bold text-danger text-uppercase mb-1">transaksi hari ini</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totaltransaksi ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-credit-card fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="container-fluid">
              <!-- <div class="col-lg-6"> -->
              <div class="table-responsive">
                <table class="display" id="DataPasien" style="width: 100%">
                  <thead>
                      <tr>
                          <th>ID Pasien</th>
                          <th>Nama Pasien</th>
                          <th>Alamat</th>
                          <th>Jenis Kelamin</th>
                          <th>Tanggal Lahir</th>
                          <th>Nomor Telepon</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tfoot>
                      <tr>
                          <th>ID Pasien</th>
                          <th>Nama Pasien</th>
                          <th>Alamat</th>
                          <th>Jenis Kelamin</th>
                          <th>Tanggal Lahir</th>
                          <th>Nomor Telepon</th>
                          <th>Action</th>
                      </tr>
                  </tfoot>
                  <tbody>
                    <?php $id = 1; ?>
                    <?php foreach ($data as $pasien) { ?>
                      <tr>
                        <td><?= $pasien[0] ?></td>
                        <td><?= $pasien[1] ?></td>
                        <td><?= $pasien[2] ?></td>
                        <td><?= $pasien[4] ?></td>
                        <td><?= $pasien[5] ?></td>
                        <td><?= $pasien[6] ?></td>
                        <td>
                          <center>
                                <a class="btn btn-sm btn-primary btn-block" href="<?= base_url('Pasien/cetak/'.$pasien[0]) ?>">Kartu Pasien</a>
                                <a class="btn btn-sm btn-danger btn-block" href="<?= base_url('Pasien/cetak_rekamedis/'.$pasien[0]) ?>">Riwayat Medis</a>
                                <a class="btn btn-sm btn-success btn-block" href="<?= base_url('Pendaftaran/add_antrian/'.$pasien[0]) ?>">Ambil Nomor Antrian</a>
                                <a class="btn btn-sm btn-info btn-block" href="<?= base_url('Pendaftaran/add_antrian/'.$pasien[0]) ?>">Catatan Medis Awal</a>
                          </center>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- </div> -->
            </div>
          </div>