          <div class="row">


            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-sm font-weight-bold text-primary text-uppercase mb-1">Tanggal</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800" id="get-time"></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-sm font-weight-bold text-success text-uppercase mb-1">Jumlah Pasien</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $jumlahpasien ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-procedures fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-sm font-weight-bold text-danger text-uppercase mb-1">transaksi hari ini</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totaltransaksi ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-credit-card fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="container-fluid">
              <!-- <div class="col-lg-6"> -->
              <div class="table-responsive">
                <table class="display" id="DataPasien" style="width: 100%">
                  <thead>
                      <tr>
                          <th>Nama Karyawan</th>
                          <th>Tipe Karyawan</th>
                          <th>Total Transaksi</th>
                          <th>Tipe Bagi Hasil</th>
                          <th>Total Bagi Hasil</th>
                      </tr>
                  </thead>
                  <tfoot>
                      <tr>
                          <th>Nama Karyawan</th>
                          <th>Tipe Karyawan</th>
                          <th>Total Transaksi</th>
                          <th>Tipe Bagi Hasil</th>
                          <th>Total Bagi Hasil</th>
                      </tr>
                  </tfoot>
                  <tbody>
                    <?php $id = 1; ?>
                    <?php foreach ($data_petugas as $transaksi) { ?>
                      <tr>
                        <td><?= $transaksi->nama_pekerja ?></td>
                        <td><?= $transaksi->role_pekerja ?></td>
                        <td><?= $transaksi->total ?></td>
                        <td>10%</td>
                        </td>
                        <?php 
                          if($transaksi->role_pekerja == 'Dokter'){
                            $bagihasil = 10 / 100 * $transaksi->total
                            ?>
                            <td><?= $bagihasil ?></td>

                        <?php  }else{
                            $bagihasil = 20000 * $transaksi->total;
                            ?>
                            <td><?= $bagihasil ?></td>
                          <?php } ?>

                        </td>
                        <td>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- </div> -->
            </div>
          </div>