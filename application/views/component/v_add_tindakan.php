          <div class="row">
              <div class="col-lg-8">
                  <div class="container-fluid">
                    <h4 class="mb-4">Add Tindakan</h4>
                      <form method="post" action="<?= base_url('Tindakan/add_master') ?>">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Nama Tindakan</label>
                          <input type="text" name="nama_tindakan" class="form-control" placeholder="ex: bersihkan karang gigi" id="exampleInputEmail1" aria-describedby="emailHelp">
                        </div>
                        <div class="form-group">
                          <label for="exampleCheck1">Biaya Tindakan</label>
                          <input type="text" name="biaya_tindakan" placeholder="150.000" class="form-control number" id="exampleCheck1">
                        </div>
                        <button type="submit" class="btn btn-md btn-primary">Submit</button>
                      </form>
                  </div>
              </div>
          </div>