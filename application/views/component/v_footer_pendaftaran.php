      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="/newdental/template/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="/newdental/template/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="/newdental/template/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="/newdental/template/vendor/chart.js/Chart.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.jsdelivr.net/npm/js-html2pdf@1.1.4/lib/html2pdf.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
  <!-- Page level custom scripts -->
  <script src="/newdental/template/js/demo/chart-area-demo.js"></script>
  <script src="/newdental/template/js/demo/chart-pie-demo.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
        $('#get-time').html(function(){
          var date = new Date();
          var bulan = date.getMonth();
          var tanggal = date.getDate();
          var hari = date.getDay();
          var tahun = date.getFullYear();
          var day = [
                  "Minggu",
                  "Senin",
                  "Selasa",
                  "Rabu",
                  "Kamis",
                  "Jum'at",
                  "Sabtu" 
                ]

          return day[hari] + ", " + tanggal + "-" + bulan + 1 + "-" + tahun;
        });
    } );

    function showModal(id, data, title, name){
      $('#modalanamnesa').modal('show')
      $('#form-anamnesa').removeAttr("action");
      $('#input-modal').attr("placeholder", data);
      $('#input-modal').attr("name", name);
      $('#TitleModal').html(title);
      $('#form-anamnesa').attr("action", "<?= base_url() ?>" + id);
    }

    function showModal2(id){

      $('#modalobat').modal('show')
      $('#form-anamnesa').removeAttr("action");
      $('#form-obat').attr("action", "<?= base_url() ?>" + id);
      $.ajax({
        url: '<?= base_url().'Obat/master_obat' ?>',
        method: 'GET',
        success: function(data){
          var html = '';
          // console.log(data);
          data.forEach(function(master, index){
            html += '<option value="' + master.id_obat +'"> ' + master.warna_obat + '</option>'
            // console.log(master.warna_obat)
          })

          $('#SelectMaster').html(html)

        },
        dataType: 'json'
      });

      $('select').on('change', function() {
        $.ajax({
          url: "<?= base_url().'Obat/getmaster_obat/' ?>" + this.value,
          method: 'GET',
          success: function(dataobat){
          var html2 = '';
            dataobat.forEach(function(masterobat, index){
              html2 += '<option value="'+ masterobat.id_obat +'">' + masterobat.nama_obat + '</option>'
            })

            $('#SelectObat').html(html2)
          },
          dataType: 'json'
        })
      });
    }

    function add_master(master){
      $("#masterObat").html(master.warna_obat);
    }

    <?php if(!empty($this->session->sukses_add) && $this->session->nomor_antrian != '0'){?>

              Swal.fire(
                'success !!',
                'Nomor Antrian: ' + <?= $this->session->nomor_antrian?>,
                'success'
              );

      <?php 
          }elseif (!empty($this->session->sukses_add) && $this->session->sukses_add == 'gagal') { ?>
                

              Swal.fire(
                'gagal !!',
                'Gagal mengambil nomor antrian',
                'error'
              );


  <?php   } ?>
  </script>

</body>

</html>