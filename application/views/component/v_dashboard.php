          <div class="row">

            <!-- Modal Anamnesa -->
              <div class="modal fade" id="modalanamnesa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="TitleModal">Anamnesa</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form method="post" action="" id="form-anamnesa">
                        <textarea name="anamnesa" placeholder="Anamnesa" rows="6" class="form-control"></textarea>
                        <button class="btn btn-primary mt-3" type="submit">Submit</button>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>

              <!-- Modal Tindakan -->

              <div class="modal fade" id="modalTindakan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Tindakan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form method="post" action="" id="form-tindakan">
                        <div class="form-group">
                          <select class="form-control" name="tindakan" onclick="pilihTindakan()">
                            <option value="">Pilih Tindakan</option>
                          </select>
                        </div>

                        <button class="btn btn-primary" type="submit">Submit</button>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>

              <!-- Modal Obat -->
              <div class="modal fade" id="modalobat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="TitleModalObat">Obat Pasien</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form method="post" action="" id="form-obat">
                        <div class="table-responsive">
                          <table class="display table" id="#dataObat">
                            <thead>
                              <tr>
                                <th>Nama Obat</th>
                                <th>Dosis</th>
                              </tr>
                            </thead>
                            <tbody id="tableData">
                            </tbody>
                          </table>
                        </div>
                        <div class="">
                          
                        </div>
                        <button class="btn btn-primary" type="submit">Submit</button>
                        <a class="btn btn-success" id="ButtonTambah" onclick="ButtonTambah()"><font color="white">Tambah Obat</font></a>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-sm font-weight-bold text-primary text-uppercase mb-1">Tanggal</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800" id="get-time"></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-sm font-weight-bold text-success text-uppercase mb-1">Jumlah Pasien</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $jumlahpasien ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-procedures fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-sm font-weight-bold text-danger text-uppercase mb-1">transaksi hari ini</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totaltransaksi ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-credit-card fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="container-fluid">
              <!-- <div class="col-lg-6"> -->
              <div class="table-responsive">
                <table class="display table" id="DataAntrian" style="width: 100%">
                  <thead>
                      <tr>
                          <th>Nama Pasien</th>
                          <th>Nomor Antrian</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tfoot>
                      <tr>
                          <th>Nama Pasien</th>
                          <th>Nomor Antrian</th>
                          <th>Action</th>
                      </tr>
                  </tfoot>
                  <tbody>
                    <?php $id = 1; ?>
                    <?php foreach ($data as $antrian) { ?>
                      <tr>
                        <td><?= $antrian[1] ?></td>
                        <td><?= $antrian[2] ?></td>
                        <td align="center">

                          <!-- Button Anamnesa -->
                          <?php if($antrian[3] == 1){ ?>
                            <a class=" btn btn-primary btn-sm" href="#">Anamnesa <i class="fa fa-check"></i></a>
                          <?php }else{ ?>
                            <button class="btn btn-primary btn-sm" onclick="showModalAnamnesa('Anamnesa/add_anamnesa/<?= $antrian[0] ?>')">Anamnesa <i class="fa fa-check" style="color:#2e59d9"></i></button>
                          <?php } ?>

                          <!-- Button Tindakan -->

                          <?php if($antrian[4] == 1){ ?>
                            <a class="btn btn-success btn-sm" href="#">Tindakan <i class="fa fa-check"></i></a>
                          <?php }else{ ?>
                            <button class="btn btn-success btn-sm" onclick="showModalTindakan('Tindakan/add_tindakan/<?= $antrian[0] ?>')">Tindakan <i class="fa fa-check" style="color:#17a673"></i></button>
                          <?php } ?>

                          <!-- Button Obat -->  

                          <?php if($antrian[5] == 1){ ?>
                            <a class="btn btn-danger btn-sm" href="#">Pilih obat <i class="fa fa-check"></i></a>
                          <?php }else{ ?>
                            <button class="btn btn-danger btn-sm" onclick="showModal2('Obat/add_obat_pasien/<?= $antrian[0] ?>', <?= $antrian[0] ?>)">Pilih obat <i class="fa fa-check" style="color:#e02d1b"></i></button>
                          <?php } ?>
                            
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- </div> -->
            </div>
          </div>