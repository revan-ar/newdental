          <div class="row">
              <div class="col-lg-8">
                  <div class="container-fluid">
                    <h4 class="mb-4">Profile</h4>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Nama Dental</label>
                          <input class="form-control" value="<?= $data_dental->nama_dental ?>" disabled>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputPassword1">Alamat Dental</label>
                          <textarea class="form-control"disabled><?= $data_dental->alamat_dental ?></textarea>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputPassword1">Username</label>
                          <input class="form-control" value="<?= $data_dental->username ?>" disabled>
                        </div>
                        <div class="form-group">
                          <label for="exampleCheck1">No. Telepon</label>
                          <input class="form-control number" value="<?= $data_dental->no_telp ?>" disabled>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputPassword1">Email</label>
                          <input class="form-control" value="<?= $data_dental->email ?>" disabled>
                        </div>
                        <button type="submit" class="btn btn-md btn-primary">Submit</button>
                  </div>
              </div>
          </div>