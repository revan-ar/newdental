	<main id="dc-main" class="dc-main dc-haslayout dc-innerbgcolor">
		<!--Register Form Start-->
		<div class="dc-haslayout dc-main-section">
			<div class="container">
				<div class="row justify-content-md-center">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 push-lg-2">
						<div class="dc-registerformhold">
							<div class="dc-registerformmain">
								<div class="dc-registerhead">
									<div class="dc-title">
										<h3>Pendaftaran Pasien</h3>
									</div>
								</div>
								<div class="dc-joinforms">
									<form class="dc-formtheme dc-formregister" method="post" action="<?= base_url('Pendaftaran/add_pasien') ?>">
										<fieldset class="dc-registerformgroup">
											<div class="form-group form-group-half">
												<input type="text" name="nama_pasien" class="form-control" placeholder="Nama Pasien">
											</div>
											<div class="form-group form-group-half">
												<input type="text" name="nama_ibu" class="form-control" placeholder="Nama Ibu">
											</div>
											<div class="form-group form-group-half">
												<select name="kelamin" style="width: 100%">
													<option value="Laki - Laki">Laki - Laki</option>
													<option value="Perempuan">Perempuan</option>
												</select>
											</div>
											<div class="form-group">
												<textarea type="text" name="alamat" class="form-control" placeholder="Alamat"></textarea>
											</div>
							                <div class="form-group">
							                	<span class="mr-2">Agama:</span>
								                  <input type="checkbox" value="Islam" name="agama" id="Islam" placeholder="Alamat Dental">
									                  <label for="Islam">Islam</label>
								                  <input type="checkbox" value="Katholik" name="agama" id="Katholik" placeholder="Alamat Dental">
									                  <label for="Katholik">Katholik</label>
  								                  <input type="checkbox" value="Protestan" name="agama" id="Protestan" placeholder="Alamat Dental">
									                  <label for="Protestan">Protestan</label>
								                  <input type="checkbox" value="Budha" name="agama" id="Budha" placeholder="Alamat Dental">
									                  <label for="Budha">Budha</label>
  								                  <input type="checkbox" value="Hindu" name="agama" id="Hindu" placeholder="Alamat Dental">
									                  <label for="Hindu">Hindu</label>
							                </div>
											<div class="form-group form-group-half">
												<input type="number" name="umur" class="form-control number" placeholder="Umur">
											</div>
											<div class="form-group form-group-half">
												<input type="number" name="kontak" class="form-control number" placeholder="No.Telp">
											</div>
											<div class="form-group form-group-half">
												<input type="number" name="telp_keluarga" class="form-control number" placeholder="No. Telp Keluarga">
											</div>
											<div class="form-group">
												<input type="date" name="tgl_lahir" class="form-control" placeholder="ex: 2020-01-01">
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-primary">Submit</button>
											</div>
										</fieldset>
									</form><br>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</main>
