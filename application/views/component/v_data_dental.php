          <div class="row">

            <div class="container-fluid">
              <!-- <div class="col-lg-6"> -->
              <div class="table-responsive">
                <table class="display table" id="dataVerif" style="width: 100%">
                  <thead>
                      <tr>
                          <th>ID Dental</th>
                          <th>Nama Dental</th>
                          <th>Nama Pemilik</th>
                          <th>Nomor Telepon</th>
                          <th>Email</th>
                          <th>Alamat Dental</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tfoot>
                      <tr>
                          <th>ID Dental</th>
                          <th>Nama Dental</th>
                          <th>Nama Pemilik</th>
                          <th>Nomor Telepon</th>
                          <th>Email</th>
                          <th>Alamat Dental</th>
                          <th>Action</th>
                      </tr>
                  </tfoot>
                  <tbody>
                    <?php $id = 1; ?>
                    <?php foreach ($data_dental as $dataDental) { ?>
                      <tr>
                        <td><?= $id++ ?></td>
                        <td><?= $dataDental->nama_dental ?></td>
                        <td><?= $dataDental->nama_owner ?></td>
                        <td><?= $dataDental->no_telp ?></td>
                        <td><?= $dataDental->email ?></td>
                        <td><?= $dataDental->alamat_dental ?></td>
                        <td align="center">

                          <!-- Button Verifikasi -->
                          <?php if($dataDental->status_verifikasi == 0){ ?>
                            <a class="btn btn-success btn-sm" href="<?= base_url('Dashboard/verif_dental/'.$dataDental->id_dental) ?>">Verifikasi</A>
                          <?php } ?>                            
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- </div> -->
            </div>
          </div>