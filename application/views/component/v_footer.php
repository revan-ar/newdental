      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="<?= base_url('Auth/logout') ?>">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="/newdental/template/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="/newdental/template/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="/newdental/template/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
  <script src="/newdental/template/vendor/chart.js/Chart.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.jsdelivr.net/npm/js-html2pdf@1.1.4/lib/html2pdf.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
  <!-- Page level custom scripts -->
  <script src="/newdental/template/js/demo/chart-area-demo.js"></script>
  <script src="/newdental/template/js/demo/chart-pie-demo.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {

      var url = document.URL;
      var suburl = url.substring(27);
      console.log(suburl);

      if(suburl === 'Dashboard'){

        $('#MenuDashboard').addClass("active");

      }else if(suburl === 'Obat'){

        $('#MenuObat').addClass("active");

      }else if(suburl === 'Petugas'){

        $('#MenuPetugas').addClass("active");

      }else if(suburl === 'Pasien'){

        $('#MenuPasien').addClass("active");

      }else if(suburl === 'Pendaftaran'){

        $('#MenuPendaftaran').addClass("active");

      }else if(suburl === 'Dashboard/add_tindakan'){

        $('#MenuAddTindakan').addClass("active");

      }else if(suburl === 'Petugas/add_petugas'){

        $('#MenuAddPetugas').addClass("active");
        // console.log(suburl);

      }else if(suburl === 'Dashboard/verifikasi'){

        $('#MenuVerifikasi').addClass("active");

      }else if(suburl === 'Petugas/add_petugas'){

        $('#MenuAddPetugas').addClass("active");
        // console.log(suburl);

      }else if(suburl === 'Transaksi'){

        $('#MenuTransaksi').addClass("active");
        // console.log(suburl);

      }

        $('#DataAntrian').DataTable({
          "order": [[2, "ASC"]]
        });

        $('#DataPasien').DataTable().order();


        $('#dataObat').DataTable();
        $('#DataTransaksi').DataTable();
        $('#dataVerif').DataTable();
        $('#DataPetugas').DataTable();

        $('#get-time').html(function(){
          var date = new Date();
          var bulan = date.getMonth();
          var tanggal = date.getDate();
          var hari = date.getDay();
          var tahun = date.getFullYear();
          var day = [
                  "Minggu",
                  "Senin",
                  "Selasa",
                  "Rabu",
                  "Kamis",
                  "Jum'at",
                  "Sabtu"
                ]
          var bulan = bulan + 1;

          return day[hari] + ", " + tanggal + "-" + bulan + "-" + tahun;
        });
    } );

        function ButtonTambah(id){
          var idSelect = id +1;
          $('#ButtonTambah').attr("onclick", "ButtonTambah(" + idSelect + ")");

          $('#tableData').append('<tr><td><div class="form-group"><select id="' + idSelect +'" name="obat_pasien[]" class"form-control"><option selected>-- Nama Obat</option></select></div></td><td><input type="text" name="dosis[]" class="form-control"></td></tr>');
          $.ajax({
            url: '<?= base_url().'Obat/getmaster_obat' ?>',
            method: 'GET',
            success: function(data){
              var html = '';
              // console.log(data);
              data.forEach(function(master, index){
                html += '<option value="' + master.id_obat +'"> ' + master.nama_obat + '</option>'
                // console.log(master.warna_obat)
              })

              // var id = 0;
              // $("select[name='obat_pasien[]']").empty();
              // var celect = $("select[name='obat_pasien[]']");
              $("select[id='" + idSelect + "']").html(html);
              // console.log(test);

            },
            dataType: 'json'
          });
        }

        $(function(){
           $(".number").keypress(function (e) {
             if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
              $("#errmsg").html("Number Only").stop().show().fadeOut("slow");
              return false;
            }
           });
        });


    function ShowTransaksi(nama, alamat, obat, dosis, total_harga, tindakan, biaya_tindakan, id_antrian){
      $('#modalTransaksi').modal('show')
      $('#NamaPasien').html(nama)
      $('#AlamatPasien').html(alamat)
      $('#ObatPasien').html(obat)
      $('#DosisObat').html(dosis)
      $('#HargaObat').html(total_harga)
      $('#TindakanDokter').html(tindakan)
      $('#TotalBiaya').html(biaya_tindakan)
      $('#UrlTransaksi').attr("action", '<?= base_url('Transaksi/tr_done/') ?>' + id_antrian)
    }

    function showModalAnamnesa(id){
      $('#modalanamnesa').modal('show')
      $('#form-anamnesa').removeAttr("action");
      $('#form-anamnesa').attr("action", "<?= base_url() ?>" + id);
    }

    function showModalTindakan(id){
      $('#modalTindakan').modal('show')
      $('#form-tindakan').removeAttr("action");
      $('#form-tindakan').attr("action", "<?= base_url() ?>" + id);

      $.ajax({
        url: '<?= base_url().'Tindakan/getmaster' ?>',
        method: 'GET',
        success: function(data){
          var html = '';
          data.forEach(function(master, index){
            html += '<option value="' + master.id_master +'"> ' + master.nama_tindakan + '</option>'
          })

          $("select[name='tindakan']").append(html)

        },
        dataType: 'json'
      });
    }

    function showModal2(id, idData){

      $('#modalobat').modal('show')
      // $('#form-anamnesa').removeAttr("action");
      $('#form-obat').attr("action", "<?= base_url() ?>" + id);
      $('#ButtonTambah').attr("onclick", "ButtonTambah(" + idData + ")");
      $('select').on('change', function() {
        $.ajax({
          url: "<?= base_url().'Obat/getmaster_obat/' ?>" + this.value,
          method: 'GET',
          success: function(dataobat){
          var html2 = '';
            dataobat.forEach(function(masterobat, index){
              html2 += '<option value="'+ masterobat.id_obat +'">' + masterobat.nama_obat + '</option>'
            })

            $('#SelectObat').append(html2)
          },
          dataType: 'json'
        })
      });
    }

    function add_master(master){
      $("#masterObat").html(master.warna_obat);
    }

    <?php if($this->session->flashdata() != null) {

              if($this->session->flashdata('sukses_add') == 'sukses' && empty($this->session->flashdata('nomor_antrian'))){ ?>

                  Swal.fire(
                    'success !!',
                    'Submit data berhasil',
                    'success'
                  );


                  <?php }else if($this->session->flashdata('sukses_add') == 'sukses' && $this->session->flashdata('nomor_antrian') != '0'){ ?>

                        Swal.fire(
                          'success !!',
                          'Nomor antrian: ' + <?= $this->session->flashdata('nomor_antrian') ?>,
                          'success'
                        );


                  <?php }else if($this->session->flashdata('sukses_add') == 'gagal' && empty($this->session->flashdata('nomor_antrian'))){ ?>

                        Swal.fire(
                          'gagal !!',
                          'Submit data gagal',
                          'error'
                        );                          


                  <?php } ?>

      <?php
          }

    ?>
  </script>

</body>

</html>