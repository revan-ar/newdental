<html class="no-js" lang=""> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Dental - Info Kesehatan, Booking dan Chat Dokter</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/normalize.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/font-awesome.min.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/fontawesome/fontawesome-all.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/linearicons.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/themify-icons.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/fullcalendar.min.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/slick.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/jquery-ui.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/prettyPhoto.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/chosen.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/main.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/responsive.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/transitions.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/doctor-slider.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/modal.min.css">
	<script src="<?= base_url('assets/landing/') ?>js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body class="dc-home" style="background-image: url(<?= base_url('template/img/undraw_doctors_hwty.png') ?>); background-repeat: no-repeat; background-size: cover; background-position: center center">
	<main id="dc-main" class="dc-main dc-haslayout">
		<!--Register Form Start-->
		<div class="dc-haslayout dc-main-section">
			<div class="container">
				<div class="row justify-content-md-center">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 push-lg-2">
						<div class="dc-registerformhold rounded" style="opacity: 80%; background-color: #dbdbdb">
							<div class="dc-registerformmain">
								<div class="dc-registerhead">
									<div class="dc-title">
										<h3>Login for Dental</h3>
									</div>
								</div>
								<div class="dc-joinforms">
									<form class="dc-formtheme dc-formregister" method="post" action="<?= base_url('auth/action_login') ?>">
										<fieldset class="dc-registerformgroup">
											
											<div class="form-group">
												<input type="text" name="username" class="form-control" placeholder="Username">
											</div>
											<div class="form-group">
												<input type="password" name="password" class="form-control" placeholder="Password">
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-lg btn-primary btn-block">Login</button>
											</div>
										</fieldset>
									</form>
								</div>
							</div>
							<div class="dc-registerformfooter">
								<span><a href="<?= base_url('auth/reset_password') ?>">Reset Password</a></span>
								<span><a href="<?= base_url('auth/register') ?>">Register</a></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</main>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/modal.min.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/vendor/jquery-3.3.1.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/vendor/jquery-library.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/vendor/bootstrap.min.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/moment.min.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/fullcalendar.min.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/owl.carousel.min.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/chosen.jquery.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/scrollbar.min.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/jquery-ui.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/prettyPhoto.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/jquery-ui.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/slick.min.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/appear.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/jRate.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/main.js"></script>
</body>
</html>