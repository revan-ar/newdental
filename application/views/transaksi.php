

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php
    if($this->session->role_user == "Dokter"){
      include 'application/views/component/v_sidebar_dokter.php'; 
    }elseif($this->session->role_user == "Administrator"){
      include 'application/views/component/v_sidebar_admin.php';
    }elseif ($this->session->role_user == "Petugas") {
      include 'application/views/component/v_sidebar_petugas.php';
    }elseif ($this->session->role_user == "Owner") {
      include 'application/views/component/v_sidebar.php';
    }

    ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
    <?php include 'application/views/component/navbar.php'; ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">


          <!-- Content Row -->
    <?php include 'application/views/component/v_transaksi.php'; ?>

          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
            </div>
          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Content Column -->
            <div class="col-lg-6 mb-4">
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->