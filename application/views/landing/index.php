<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Dental - Info Kesehatan, Booking dan Chat Dokter</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<link rel="icon" href="<?= base_url('assets/landing/') ?>images/favicon.png" type="image/x-icon">

	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/normalize.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/font-awesome.min.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/fontawesome/fontawesome-all.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/linearicons.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/themify-icons.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/fullcalendar.min.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/slick.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/jquery-ui.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/prettyPhoto.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/chosen.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/main.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/responsive.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/transitions.css">
	<link rel="stylesheet" href="<?= base_url('assets/landing/') ?>css/doctor-slider.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/modal.min.css">
	<script src="<?= base_url('assets/landing/') ?>js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body class="dc-home">

	<div class="preloader-outer">
		<div class="wt-preloader-holder">
			<div class="wt-loader"></div>
		</div>
	</div>

	<div id="dc-wrapper" class="dc-wrapper dc-haslayout">
		<header id="dc-header" class="dc-header dc-haslayout">
			<div class="dc-topbar" style="background-color: #20AAE0">
				<div class="container">
					<div class="row">
						<div class="col-12 col-sm-12 col-md-12 col-lg-12" style="align-items: center;">
							<div class="dc-helpnum">
								<span>Customer Support</span>
								<a href="tel:123456789" style="color: white"><i class="fas fa-phone-volume"></i> 085325115407</a>
							</div>
							<div class="dc-rightarea">
								<a style="border: 1px solid white;" href="javascript:void(0);" id="tentang" class="dc-borderku">Tentang</a>
								<a style="border: 1px solid white;" href="<?= base_url('auth/login') ?>" class="dc-borderku">Masuk</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>

		<div class="dc-homesliderholder dc-haslayout">
			<div id="dc-homeslider" class="dc-homeslider">
				<div id="dc-bannerslider" class="dc-bannerslider carousel slide" data-ride="false" data-interval="false">
				  <ol class="carousel-indicators dc-bannerdots">
				    <li data-target="#dc-bannerslider" data-slide-to="0" class="active"></li>
				    <li data-target="#dc-bannerslider" data-slide-to="1"></li>
				    <li data-target="#dc-bannerslider" data-slide-to="2"></li>
				  </ol>
					<div class="carousel-inner">
						<div class="carousel-item active" id="carousel-item-1">
							<div class="d-flex justify-content-center dc-craousel-content">
								<div class="mx-auto">
									<img class="d-block dc-bannerimg" src="<?= base_url('assets/landing/') ?>assets/img/cartoon-png-dentistry-medicine.png" alt="First slide">
									<div class="dc-bannercontent dc-bannercotent-craousel" >
										<div class="dc-content-carousel">
											<div class="dc-num"></div>
											<h1><em>Registrasi.</em> Dengan Handphone<span> kamu bisa langsung mendaftar.</span></h1>
											
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="carousel-item" id="carousel-item-2">
							<div class="d-flex justify-content-center dc-craousel-content">
								<div class="mx-auto">
									<img class="d-block dc-bannerimg" src="<?= base_url('assets/landing/') ?>assets/img/dentistry-png-cosmetic-clipart.png" alt="Second slide">
									<div class="dc-bannercontent dc-bannercotent-craousel" >
										<div class="dc-content-carousel">
											<div class="dc-num"></div>
											<h1><em>Pengecekan</em> Kamu langsung<span> Bisa melakukan pengecekan lohhh</span></h1>
											
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="carousel-item" id="carousel-item-3">
							<div class="d-flex justify-content-center dc-craousel-content">
								<div class="mx-auto">
									<img class="d-block dc-bannerimg" src="<?= base_url('assets/landing/') ?>assets/img/dental-patient-cartoon-png-dentistry-clipart-5bfebe8435c62e9b.png" alt="Third slide">
									<div class="dc-bannercontent dc-bannercotent-craousel" >
										<div class="dc-content-carousel">
											<div class="dc-num"></div>
											<h1><em>Finaly</em> Yeay, Kamu Sehat<span> Lebih sehat tanpa Ribet lagi.</span></h1>
											<div class="dc-btnarea">
												<a href="<?= base_url('auth/register') ?>" class="dc-btn dc-btnactive">Register</a>
												<a href="<?= base_url('auth/login') ?>" class="dc-btn">Login</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<a class="dc-carousel-control-prev" href="#dc-bannerslider" role="button" data-slide="prev">
							<span class="dc-carousel-control-prev-icon" aria-hidden="true" style="color: #20AAE0"><span>PR</span><span class="d-block">EV</span></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="dc-carousel-control-next" href="#dc-bannerslider" role="button" data-slide="next">
							<span class="dc-carousel-control-next-icon " aria-hidden="true" style="color: #20AAE0"><span>NE</span><span class="d-block">XT</span></span>
							<span class="sr-only">Next</span>
						</a>
					</div>				
				</div>
			</div>
		</div>
	</div>

	<div class="modal" tabindex="-1" role="dialog" id="modal">
	  <div class="modal-dialog modal-lg" role="document">
		    <div class="ui cards">
			  	<div class="card" style="width: 100%; margin-top: 100px">
				    <div class="content">
				    	<div class="header">Tentang Cloud Klinik</div>
				    	<div class="meta">2020-02-10</div>
					    <div class="description">
					    	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					    </div>
				    </div>
				</div>
			</div>
	  </div>
	  
	</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/modal.min.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/vendor/jquery-3.3.1.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/vendor/jquery-library.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/vendor/bootstrap.min.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/moment.min.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/fullcalendar.min.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/owl.carousel.min.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/chosen.jquery.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/scrollbar.min.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/jquery-ui.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/prettyPhoto.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/jquery-ui.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/slick.min.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/appear.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/jRate.js"></script>
	<script src="<?= base_url('assets/landing/') ?>js/main.js"></script>
	<script>
		$('#tentang').on('click', () => {
			$('#modal').modal('show')
		})
	</script>
</body>
</html>