<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Obat_model extends CI_Model {

	public function add_master($data){
		$input = $this->db->insert('master_obat', $data);

		if($input){
			return "add master obat berhasil";
		}else{
			return "add master obat gagal";
		}
	}

	public function add_data($data){
		$input = $this->db->insert('data_obat', $data);

		if($input){
			return "add data obat berhasil";
		}else{
			return "add data obat gagal";
		}
	}

	public function add_obat_pasien($data){
		$input = $this->db->insert('obat_pasien', $data);

		if($input){
			return "add obat pasien berhasil";
		}else{
			return "add obat pasien gagal";
		}	
	}
}
