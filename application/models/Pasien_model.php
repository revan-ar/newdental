<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien_model extends CI_Model {

	public function add_anamnesa($data){
		$anamnesa = $this->db->insert('anamnesa', $data);

		if($anamnesa == TRUE){

			return "add anamnesa berhasil";
			
		}else{

			return "add anamnesa gagal";

		}

	}

	public function add_tindakan($data){
		$add =$this->db->insert('tindakan', $data);

		if($add){
			return "add tindakan berhasil";
		}else{
			return "add tindakan gagal";
		}

	}
}
