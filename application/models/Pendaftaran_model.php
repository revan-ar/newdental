<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pendaftaran_model extends CI_Model {

	public function add_dental($data){
		$add = $this->db->insert('data_dental', $data);

		if($add){
			return "add data berhasil";
		}else{
			return "add data gagal";
		}
	}

	public function add_user($data){
		$add = $this->db->insert('user', $data);

		if($add){
			return "add data berhasil";
		}else{
			return "add data gagal";
		}
	}



	public function add_data($data){
		$add = $this->db->insert('pasien', $data);

		if($add){
			return "add data berhasil";
		}else{
			return "add data gagal";
		}
	}

	public function add_antrian($data){
		$add = $this->db->insert('antrian', $data);
		
		if($add){
			return "add antrian berhasil";
		}else{
			return "add antrian gagal";
		}
	}
}
