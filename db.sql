-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 07 Feb 2020 pada 20.07
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dental`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `anamnesa`
--

CREATE TABLE `anamnesa` (
  `id_anamnesa` int(11) NOT NULL,
  `id_dental` int(20) NOT NULL,
  `id_antrian` int(20) NOT NULL,
  `anamnesa` text NOT NULL,
  `input_tgl` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `anamnesa`
--

INSERT INTO `anamnesa` (`id_anamnesa`, `id_dental`, `id_antrian`, `anamnesa`, `input_tgl`) VALUES
(1, 1, 1, 'test anamnesa\r\n', '2020-02-04 01:10:55'),
(2, 1, 2, 'test anamnesa', '2020-02-06 08:27:46'),
(3, 1, 2, 'test', '2020-02-06 08:57:27'),
(4, 1, 1, 'test anamnesa', '2020-02-06 09:00:15'),
(5, 1, 4, 'test anamnesa', '2020-02-06 09:03:14'),
(6, 1, 9, 'test anamnesa', '2020-02-06 09:03:25'),
(7, 1, 8, 'testt', '2020-02-06 09:14:44'),
(8, 1, 5, 'test', '2020-02-06 09:14:57'),
(9, 1, 6, 'test anamnesa', '2020-02-07 04:49:01'),
(10, 1, 3, 'tet', '2020-02-07 04:49:57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `antrian`
--

CREATE TABLE `antrian` (
  `id_antrian` int(10) NOT NULL,
  `id_dental` int(50) NOT NULL,
  `id_pasien` int(10) NOT NULL,
  `nama_pasien` varchar(30) DEFAULT NULL,
  `anamnesa` enum('0','1') NOT NULL DEFAULT '0',
  `tindakan` enum('0','1') NOT NULL DEFAULT '0',
  `obat` enum('0','1') NOT NULL,
  `nomor_antrian` int(100) NOT NULL,
  `input_tgl` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `antrian`
--

INSERT INTO `antrian` (`id_antrian`, `id_dental`, `id_pasien`, `nama_pasien`, `anamnesa`, `tindakan`, `obat`, `nomor_antrian`, `input_tgl`) VALUES
(1, 1, 1, 'Iqbal Nur', '1', '1', '0', 1, '2020-02-04 01:09:32'),
(2, 1, 1, 'Iqbal Nur', '1', '0', '0', 1, '2020-02-05 07:56:44'),
(3, 1, 1, 'Iqbal Nur', '1', '0', '1', 1, '2020-02-06 02:20:52'),
(4, 1, 2, 'Revan A', '1', '0', '0', 2, '2020-02-06 06:42:51'),
(5, 1, 2, 'Revan A', '1', '1', '0', 3, '2020-02-06 06:43:16'),
(6, 1, 2, 'Revan A', '1', '0', '0', 4, '2020-02-06 06:43:22'),
(7, 1, 2, 'Revan A', '0', '0', '0', 5, '2020-02-06 06:43:24'),
(8, 1, 2, 'Revan A', '1', '0', '0', 6, '2020-02-06 06:43:25'),
(9, 1, 2, 'Revan A', '1', '0', '0', 7, '2020-02-06 06:43:25'),
(10, 1, 2, 'Revan A', '0', '0', '0', 8, '2020-02-06 06:43:26'),
(11, 1, 2, 'Revan A', '0', '0', '0', 9, '2020-02-06 06:43:26'),
(12, 1, 2, 'Revan A', '0', '0', '0', 10, '2020-02-06 06:43:27'),
(13, 1, 2, 'Revan A', '0', '0', '1', 11, '2020-02-06 06:43:27'),
(14, 1, 2, 'Revan A', '0', '0', '0', 12, '2020-02-06 06:43:28'),
(15, 1, 2, 'Revan A', '0', '0', '0', 13, '2020-02-06 06:43:29'),
(16, 1, 2, 'Revan A', '0', '0', '0', 14, '2020-02-06 06:43:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_obat`
--

CREATE TABLE `data_obat` (
  `id_obat` int(10) NOT NULL,
  `id_dental` int(10) NOT NULL,
  `id_jenis_obat` int(10) NOT NULL,
  `nama_obat` varchar(50) NOT NULL,
  `warna_obat` varchar(20) NOT NULL,
  `stock_obat` int(10) NOT NULL,
  `input_tgl` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `data_obat`
--

INSERT INTO `data_obat` (`id_obat`, `id_dental`, `id_jenis_obat`, `nama_obat`, `warna_obat`, `stock_obat`, `input_tgl`) VALUES
(1, 1, 1, 'praxion - paracetamol', 'hijau', 299, '2020-02-04 00:00:00'),
(2, 1, 1, 'amoxicillin', 'hijau', 299, '2020-02-04 00:00:00'),
(3, 1, 2, 'test obat', 'merah', 87, '0000-00-00 00:00:00'),
(4, 1, 3, 'test obat 2', 'biru', 88, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_obat`
--

CREATE TABLE `master_obat` (
  `id_obat` int(10) NOT NULL,
  `id_dental` int(20) NOT NULL,
  `id_jenis_obat` int(30) NOT NULL,
  `warna_obat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `master_obat`
--

INSERT INTO `master_obat` (`id_obat`, `id_dental`, `id_jenis_obat`, `warna_obat`) VALUES
(1, 1, 1, 'hijau'),
(2, 1, 2, 'merah'),
(3, 1, 3, 'biru');

-- --------------------------------------------------------

--
-- Struktur dari tabel `obat_pasien`
--

CREATE TABLE `obat_pasien` (
  `id_obat` int(11) NOT NULL,
  `id_dental` int(11) NOT NULL,
  `id_pasien` int(11) NOT NULL,
  `nama_pasien` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `obat` varchar(100) NOT NULL,
  `tgl_berobat` date NOT NULL,
  `input_tgl` datetime NOT NULL,
  `input_oleh` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `obat_pasien`
--

INSERT INTO `obat_pasien` (`id_obat`, `id_dental`, `id_pasien`, `nama_pasien`, `alamat`, `obat`, `tgl_berobat`, `input_tgl`, `input_oleh`) VALUES
(1, 1, 1, 'Iqbal Nur', 'jl Timun mas', 'praxion - paracetamol,amoxicillin,test obat', '2020-02-05', '2020-02-05 08:39:02', 'revan.ar'),
(2, 1, 1, 'Iqbal Nur', 'jl Timun mas', 'amoxicillin,test obat,test obat 2', '2020-02-06', '2020-02-06 07:14:31', 'revan.ar'),
(3, 1, 1, 'Iqbal Nur', 'jl Timun mas', 'praxion - paracetamol,amoxicillin,test obat', '2020-02-06', '2020-02-06 08:28:03', 'revan.ar'),
(4, 1, 1, 'Iqbal Nur', 'jl Timun mas', 'amoxicillin,test obat,test obat 2', '2020-02-06', '2020-02-06 08:57:06', 'revan.ar'),
(5, 1, 1, 'Iqbal Nur', 'jl Timun mas', 'praxion - paracetamol,amoxicillin,test obat', '2020-02-06', '2020-02-06 08:58:02', 'revan.ar'),
(6, 1, 1, 'Iqbal Nur', 'jl Timun mas', 'praxion - paracetamol,amoxicillin,test obat 2', '2020-02-06', '2020-02-06 09:13:03', 'revan.ar'),
(7, 1, 2, 'Revan A', 'jl MT Haryono', 'amoxicillin,test obat,test obat 2', '2020-02-06', '2020-02-06 09:13:31', 'revan.ar'),
(8, 1, 2, 'Revan A', 'jl MT Haryono', 'praxion - paracetamol,test obat,test obat 2', '2020-02-06', '2020-02-06 09:15:10', 'revan.ar'),
(9, 1, 2, 'Revan A', 'jl MT Haryono', 'amoxicillin,test obat,test obat 2', '2020-02-06', '2020-02-06 09:19:51', 'revan.ar'),
(10, 1, 2, 'Revan A', 'jl MT Haryono', 'amoxicillin,test obat,test obat 2', '2020-02-06', '2020-02-06 09:20:58', 'revan.ar'),
(11, 1, 2, 'Revan A', 'jl MT Haryono', 'amoxicillin,test obat,test obat 2', '2020-02-06', '2020-02-06 09:22:59', 'revan.ar'),
(12, 1, 1, 'Iqbal Nur', 'jl Timun mas', 'amoxicillin,test obat,test obat 2', '2020-02-06', '2020-02-06 09:56:02', 'revan.ar');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pasien`
--

CREATE TABLE `pasien` (
  `id_data_pasien` int(10) NOT NULL,
  `id_pasien` int(30) NOT NULL,
  `id_dental` int(50) NOT NULL,
  `nama_pasien` varchar(150) NOT NULL,
  `alamat` text NOT NULL,
  `umur` varchar(10) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `tanggal_lahir` varchar(20) NOT NULL,
  `kontak_pasien` varchar(20) NOT NULL,
  `input_tgl` datetime NOT NULL,
  `input_oleh` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pasien`
--

INSERT INTO `pasien` (`id_data_pasien`, `id_pasien`, `id_dental`, `nama_pasien`, `alamat`, `umur`, `gender`, `tanggal_lahir`, `kontak_pasien`, `input_tgl`, `input_oleh`) VALUES
(1, 1, 1, 'Iqbal Nur', 'jl Timun mas', '17', 'Laki-laki', '29-08-2004', '085789765421', '2020-02-04 01:08:19', 'revan.ar'),
(2, 2, 1, 'Revan A', 'jl MT Haryono', '19`', 'Laki-laki', '13-02-1997', '085774686399', '2020-02-06 06:40:13', 'revan.ar');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tindakan`
--

CREATE TABLE `tindakan` (
  `id_tindakan` int(11) NOT NULL,
  `id_dental` int(11) NOT NULL,
  `id_antrian` int(11) NOT NULL,
  `tindakan` text NOT NULL,
  `input_tgl` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tindakan`
--

INSERT INTO `tindakan` (`id_tindakan`, `id_dental`, `id_antrian`, `tindakan`, `input_tgl`) VALUES
(1, 1, 1, 'test tindakan', '2020-02-04 01:11:06'),
(2, 1, 1, 'testt tindakan', '2020-02-06 08:56:50'),
(3, 1, 5, 'testt', '2020-02-06 09:24:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(20) NOT NULL,
  `id_dental` int(50) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role_user` varchar(20) NOT NULL,
  `registered_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `id_dental`, `nama`, `username`, `password`, `role_user`, `registered_at`) VALUES
(1, 1, 'Revan Arifio', 'revan.ar', '202cb962ac59075b964b07152d234b70', 'Dokter', '2020-02-03 22:13:04'),
(2, 2, 'Iqbal Nur', 'iqbal.n', '202cb962ac59075b964b07152d234b70', 'Admin', '2020-02-03 22:13:07');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `anamnesa`
--
ALTER TABLE `anamnesa`
  ADD PRIMARY KEY (`id_anamnesa`);

--
-- Indeks untuk tabel `antrian`
--
ALTER TABLE `antrian`
  ADD PRIMARY KEY (`id_antrian`);

--
-- Indeks untuk tabel `data_obat`
--
ALTER TABLE `data_obat`
  ADD PRIMARY KEY (`id_obat`);

--
-- Indeks untuk tabel `master_obat`
--
ALTER TABLE `master_obat`
  ADD PRIMARY KEY (`id_obat`);

--
-- Indeks untuk tabel `obat_pasien`
--
ALTER TABLE `obat_pasien`
  ADD PRIMARY KEY (`id_obat`);

--
-- Indeks untuk tabel `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`id_data_pasien`);

--
-- Indeks untuk tabel `tindakan`
--
ALTER TABLE `tindakan`
  ADD PRIMARY KEY (`id_tindakan`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `anamnesa`
--
ALTER TABLE `anamnesa`
  MODIFY `id_anamnesa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `antrian`
--
ALTER TABLE `antrian`
  MODIFY `id_antrian` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `data_obat`
--
ALTER TABLE `data_obat`
  MODIFY `id_obat` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `master_obat`
--
ALTER TABLE `master_obat`
  MODIFY `id_obat` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `obat_pasien`
--
ALTER TABLE `obat_pasien`
  MODIFY `id_obat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `pasien`
--
ALTER TABLE `pasien`
  MODIFY `id_data_pasien` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tindakan`
--
ALTER TABLE `tindakan`
  MODIFY `id_tindakan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
